package com.adel.ecommerce.ui.store.notification

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adel.ecommerce.service.NotificationProducts
import com.adel.ecommerce.service.WishlistProducts
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.Date
import javax.inject.Inject

@HiltViewModel
class NotificationViewModel @Inject constructor(
    application: Application
) : ViewModel() {

    private var notificationDb: NotificationDatabase? = NotificationDatabase.getDatabase(application)
    private var notificationDAO = notificationDb?.notificationDao()

    fun addToNotification(
       notifItem: NotificationProducts
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            notificationDAO?.addToNotification(notifItem)
        }
    }

    fun getNotificationItem(): LiveData<List<NotificationProducts>>? {
        return notificationDAO?.getNotificationProducts()
    }

    fun isReadNotification(isRead: Boolean, id: String) {
        CoroutineScope(Dispatchers.IO).launch {
            notificationDAO?.isReadNotification(isRead, id)
        }
    }
}