package com.adel.ecommerce.ui.store.checkout

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.adel.ecommerce.EcommerceRepository
import com.adel.ecommerce.databinding.ActivityPaymentMethodBinding
import com.adel.ecommerce.service.PaymentMethodCategoryResponse
import com.adel.ecommerce.service.PaymentMethodItemResponse
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class PaymentMethodActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPaymentMethodBinding
    private lateinit var virtualAccountAdapter: PaymentMethodsAdapter
    private lateinit var bankAdapter: PaymentMethodsAdapter
    private lateinit var instantAdapter: PaymentMethodsAdapter

    @Inject
    lateinit var repository: EcommerceRepository

    @Inject
    lateinit var analytics: FirebaseAnalytics

    @Inject
    lateinit var remoteConfig: FirebaseRemoteConfig

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityPaymentMethodBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val jsonString = remoteConfig.getString("payment")
        val type = object : TypeToken<List<PaymentMethodCategoryResponse>>() {}.type
        val paymentMethods = Gson().fromJson<List<PaymentMethodCategoryResponse>>(jsonString, type)

        Log.d("PaymentMethod", "onCreate: $paymentMethods")

        virtualAccountAdapter = PaymentMethodsAdapter()
        bankAdapter = PaymentMethodsAdapter()
        instantAdapter = PaymentMethodsAdapter()

        binding.apply {
            val title = paymentMethods.map { it.title }
            titleVa.text = title[0]
            val virtualAccountItems = paymentMethods[0].item
            Log.d("Payment Method", "virtual : $virtualAccountItems")
            rvVirtualAccount.adapter = virtualAccountAdapter // Use the same adapter instance
            virtualAccountAdapter.setPaymentMethods(ArrayList(virtualAccountItems))
            rvVirtualAccount.layoutManager = GridLayoutManager(this@PaymentMethodActivity, 1)

            titleTf.text = title[1]
            val bankItems = paymentMethods[1].item
            Log.d("Payment Method", "bank : $bankItems")
            rvTransfer.adapter = bankAdapter // Use the same adapter instance
            bankAdapter.setPaymentMethods(ArrayList(bankItems))
            rvTransfer.layoutManager = GridLayoutManager(this@PaymentMethodActivity, 1)

            titlePi.text = title[2]
            val instanItems = paymentMethods[2].item
            Log.d("Payment Method", "instan : $instanItems")
            rvPembayaranInstan.adapter = instantAdapter // Use the same adapter instance
            instantAdapter.setPaymentMethods(ArrayList(instanItems))
            rvPembayaranInstan.layoutManager = GridLayoutManager(this@PaymentMethodActivity, 1)

            virtualAccountAdapter.setOnItemClickListener(object :
                PaymentMethodsAdapter.OnItemClickListener {
                override fun onItemClicked(label: PaymentMethodItemResponse) {
                    selectPaymentMethod(label.label, label.image)

                    val addPaymentInfo = Bundle()
                    addPaymentInfo.putString(FirebaseAnalytics.Param.CURRENCY, "IDR")
                    addPaymentInfo.putString(FirebaseAnalytics.Param.PAYMENT_TYPE, label.label)
                    val params = Bundle()
                    params.putParcelableArray(
                        FirebaseAnalytics.Param.ITEMS,
                        arrayOf(addPaymentInfo)
                    )

                    analytics.logEvent(FirebaseAnalytics.Event.ADD_PAYMENT_INFO, params)
                }
            })

            bankAdapter.setOnItemClickListener(object : PaymentMethodsAdapter.OnItemClickListener {
                override fun onItemClicked(label: PaymentMethodItemResponse) {
                    selectPaymentMethod(label.label, label.image)

                    val addPaymentInfo = Bundle()
                    addPaymentInfo.putString(FirebaseAnalytics.Param.CURRENCY, "IDR")
                    addPaymentInfo.putString(FirebaseAnalytics.Param.PAYMENT_TYPE, label.label)
                    val params = Bundle()
                    params.putParcelableArray(
                        FirebaseAnalytics.Param.ITEMS,
                        arrayOf(addPaymentInfo)
                    )

                    analytics.logEvent(FirebaseAnalytics.Event.ADD_PAYMENT_INFO, params)
                }
            })

            instantAdapter.setOnItemClickListener(object :
                PaymentMethodsAdapter.OnItemClickListener {
                override fun onItemClicked(label: PaymentMethodItemResponse) {
                    selectPaymentMethod(label.label, label.image)

                    val addPaymentInfo = Bundle()
                    addPaymentInfo.putString(FirebaseAnalytics.Param.CURRENCY, "IDR")
                    addPaymentInfo.putString(FirebaseAnalytics.Param.PAYMENT_TYPE, label.label)
                    val params = Bundle()
                    params.putParcelableArray(
                        FirebaseAnalytics.Param.ITEMS,
                        arrayOf(addPaymentInfo)
                    )

                    analytics.logEvent(FirebaseAnalytics.Event.ADD_PAYMENT_INFO, params)
                }
            })
        }
    }

    private fun selectPaymentMethod(paymentMethodName: String, paymentMethodImage: String) {
        val resultIntent = Intent()
        resultIntent.putExtra(CheckoutActivity.EXTRA_PAYMENT_NAME, paymentMethodName)
        resultIntent.putExtra(CheckoutActivity.EXTRA_PAYMENT_IMAGE, paymentMethodImage)
        setResult(RESULT_OK, resultIntent)
        finish()
    }
}