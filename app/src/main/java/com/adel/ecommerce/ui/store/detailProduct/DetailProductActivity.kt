package com.adel.ecommerce.ui.store.detailProduct

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.adel.ecommerce.EcommerceRepository
import com.adel.ecommerce.R
import com.adel.ecommerce.databinding.ActivityDetailProductBinding
import com.adel.ecommerce.service.ProductVariant
import com.adel.ecommerce.service.asWishlist
import com.adel.ecommerce.session.SessionPreference
import com.adel.ecommerce.ui.prelogin.login.LoginActivity
import com.adel.ecommerce.ui.store.cart.CartActivity
import com.adel.ecommerce.ui.store.cart.CartViewModel
import com.adel.ecommerce.ui.store.checkout.CheckoutActivity
import com.adel.ecommerce.ui.store.wishlist.WishlistViewModel
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.google.android.material.badge.ExperimentalBadgeUtils
import com.google.android.material.chip.Chip
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.NumberFormat
import java.util.Locale
import javax.inject.Inject

@AndroidEntryPoint
class DetailProductActivity :
    AppCompatActivity() {
    private lateinit var binding: ActivityDetailProductBinding
    private val viewModel: WishlistViewModel by viewModels()
    private val cartViewModel: CartViewModel by viewModels()

    @Inject
    lateinit var repository: EcommerceRepository

    @Inject
    lateinit var analytics: FirebaseAnalytics

    private lateinit var rootView: View
    private var stock: Int? = null
    private var variantName: String? = null
    private var variantPrice: Int? = null

    @ExperimentalBadgeUtils
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityDetailProductBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        rootView = findViewById(android.R.id.content)
        setupToolbar()

        cartViewModel.getCartItem()?.observe(this) {
            binding.topAppBar.viewTreeObserver.addOnGlobalLayoutListener {
                val badgeDrawable = BadgeDrawable.create(this).apply {
                    isVisible = it.isNotEmpty()
                    number = it.size
                }
                BadgeUtils.attachBadgeDrawable(
                    badgeDrawable,
                    binding.topAppBar,
                    R.id.toolbar_cart
                )
            }
        }

        val id = intent.getStringExtra(EXTRA_ID_DETAIL)

        val name = intent.getStringExtra(EXTRA_NAME_PROD)
        val price = intent.getIntExtra(EXTRA_PRICE_PROD, 0)
        val store = intent.getStringExtra(EXTRA_STORE_PROD)
        val image = intent.getStringExtra(EXTRA_IMAGE_PROD)
        val sale = intent.getIntExtra(EXTRA_SALE_PROD, 0)
        val rating = intent.getFloatExtra(EXTRA_RATING_PROD, 0F)

        id?.let {
            repository.getProductDetail(it)
        }
        repository.detailProduct.observe(this) {
            if (it != null) {
                binding.apply {
                    val hargaDefault = it.productPrice
                    val harga =
                        NumberFormat.getNumberInstance(Locale("id", "ID")).format(hargaDefault)
                    tvHarga.text = StringBuilder().append("Rp").append(harga)
                    tvNamaBarang.text = it.productName
                    adsSold.text = it.sale.toString()
                    tvIsiDeskripsi.text = it.description
                    ratingBtn.text =
                        StringBuilder()
                            .append(it.productRating).append(" (").append(it.totalRating.toString())
                            .append(")")
                    tvPersenan.text =
                        StringBuilder().append(it.totalSatisfaction.toString()).append("%")
                    tvJmlRating.text =
                        StringBuilder().append(it.totalRating.toString()).append(" rating")
                    tvProductRating.text = it.productRating.toString()

                    stock = it.stock

                    val viewItemDetail = Bundle()
                    viewItemDetail.putString(FirebaseAnalytics.Param.ITEM_NAME, it.productName)
                    viewItemDetail.putString(FirebaseAnalytics.Param.CURRENCY, "IDR")
                    viewItemDetail.putString(
                        FirebaseAnalytics.Param.VALUE,
                        it.productPrice.toString()
                    )
                    val params = Bundle()
                    params.putParcelableArray(
                        FirebaseAnalytics.Param.ITEMS,
                        arrayOf(viewItemDetail)
                    )

                    analytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM, params)

                    val variantMap = mutableMapOf<Int, ProductVariant>()

                    for (variant in it.productVariant) {
                        val variantName = variant.variantName

                        val chip = Chip(chipGroupVarian.context)
                        chip.apply {
                            text = variantName
                            chip.id = View.generateViewId()
                            isChipIconVisible = false
                            isCloseIconVisible = false
                            isCheckable = true
                        }
                        chipGroupVarian.addView(chip)
                        variantMap[chip.id] = variant

                        val firstChip: Chip? = chipGroupVarian.getChildAt(0) as? Chip
                        firstChip?.isChecked = true

                    }

                    chipGroupVarian.setOnCheckedChangeListener { _, checkedId ->
                        val selectedVariant = variantMap[checkedId]

                        if (selectedVariant != null) {
                            val selectedVariantPrice = selectedVariant.variantPrice
                            val hargaAkhir = hargaDefault + selectedVariantPrice
                            val hargaAddOn = NumberFormat.getNumberInstance(Locale("id", "ID"))
                                .format(hargaAkhir)
                            tvHarga.text = StringBuilder().append("Rp")
                                .append(hargaAddOn).toString()

                            variantName = selectedVariant.variantName
                            variantPrice = selectedVariant.variantPrice

                        }
                    }

                    val imageAdapter = ImagePagerAdapter(it.image)
                    detailProductVp.adapter = imageAdapter

                    TabLayoutMediator(tabDots, detailProductVp) { _, _ ->
                    }.attach()
                }
            }

            binding.btnBeliLangsung.setOnClickListener { btn ->
                val intentToCheckout = Intent(this, CheckoutActivity::class.java)
                val itemSelected =
                    arrayListOf(it.asWishlist(variantName ?: "RAM 16", variantPrice ?: 0))
                intentToCheckout.putParcelableArrayListExtra(
                    CheckoutActivity.EXTRA_PROD,
                    ArrayList(itemSelected)
                )
                intentToCheckout.putExtra(
                    CheckoutActivity.EXTRA_SOURCE_ACTIVITY,
                    "DetailProductActivity"
                )
                startActivity(intentToCheckout)
            }
        }

        var _isChecked = false
        binding.toggleFavorite.setOnClickListener {
            _isChecked = !_isChecked
            if (_isChecked) {
                viewModel.addToWishlist(
                    id.toString(),
                    name.toString(),
                    price,
                    image.toString(),
                    store.toString(),
                    sale,
                    stock,
                    rating,
                    variantName,
                    variantPrice
                )
                Snackbar.make(rootView, "Produk ditambahkan ke wishlist", Snackbar.LENGTH_SHORT)
                    .show()

                val addtoWishlist = Bundle()
                addtoWishlist.putString(FirebaseAnalytics.Param.ITEM_ID, id)
                addtoWishlist.putString(FirebaseAnalytics.Param.ITEM_NAME, name)
                addtoWishlist.putString(FirebaseAnalytics.Param.VALUE, price.toString())
                addtoWishlist.putString(FirebaseAnalytics.Param.CURRENCY, "IDR")
                val params = Bundle()
                params.putParcelableArray(FirebaseAnalytics.Param.ITEMS, arrayOf(addtoWishlist))

                analytics.logEvent(FirebaseAnalytics.Event.ADD_TO_WISHLIST, params)
            } else {
                viewModel.removeFromWL(id.toString())
                Snackbar.make(rootView, "Produk dihapus dari wishlist", Snackbar.LENGTH_SHORT)
                    .show()
            }
            binding.toggleFavorite.isChecked = _isChecked
        }

        CoroutineScope(Dispatchers.IO).launch {
            val count = viewModel.checkWl(id.toString())
            withContext(Dispatchers.Main) {
                if (count != null) {
                    if (count > 0) {
                        binding.toggleFavorite.isChecked = true
                        _isChecked = true
                    } else {
                        binding.toggleFavorite.isChecked = false
                        _isChecked = false
                    }
                }
            }
        }

        binding.btnKeKeranjang.setOnClickListener {
            cartViewModel.addToCart(
                id.toString(),
                name.toString(),
                price,
                image.toString(),
                stock,
                "RAM 16",
                0
            )
            Snackbar.make(
                rootView,
                "Produk berhasil ditambahkan ke keranjang!",
                Snackbar.LENGTH_SHORT
            ).show()

            val addtoCart = Bundle()
            addtoCart.putString(FirebaseAnalytics.Param.ITEM_ID, id)
            addtoCart.putString(FirebaseAnalytics.Param.ITEM_NAME, name)
            addtoCart.putString(FirebaseAnalytics.Param.CURRENCY, "IDR")
            val params = Bundle()
            params.putParcelableArray(FirebaseAnalytics.Param.ITEMS, arrayOf(addtoCart))

            analytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART, params)

        }

        binding.buttonLihatSemua.setOnClickListener {
            val intentToReview = Intent(this, ReviewProductActivity::class.java)
            intentToReview.putExtra(ReviewProductActivity.EXTRA_ID_REVIEW, id)
            startActivity(intentToReview)
        }

        binding.topAppBar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun setupToolbar() {
        binding.apply {
            val sessionPreference = SessionPreference(this@DetailProductActivity)
            topAppBar.title = sessionPreference.passUserName().toString()

            topAppBar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.toolbar_notifications -> {
                        val intentToNotifs =
                            Intent(this@DetailProductActivity, DetailProductActivity::class.java)
//                        startActivity(intentToNotifs)
                        true
                    }

                    R.id.toolbar_cart -> {
                        val intentToCart =
                            Intent(this@DetailProductActivity, CartActivity::class.java)
                        startActivity(intentToCart)
                        true
                    }

                    R.id.toolbar_menu -> {
                        val loginSession = SessionPreference(this@DetailProductActivity)

                        loginSession.logOut()

                        startActivity(Intent(this@DetailProductActivity, LoginActivity::class.java))
                        finish()
                        true
                    }

                    else -> false
                }
            }
        }
    }

    companion object {
        const val TAG = "DetailProductActivity"
        const val EXTRA_ID_DETAIL = "extra_id"
        const val EXTRA_NAME_PROD = "extra_name"
        const val EXTRA_IMAGE_PROD = "extra_image"
        const val EXTRA_PRICE_PROD = "extra_price"
        const val EXTRA_SALE_PROD = "extra_sale"
        const val EXTRA_RATING_PROD = "extra_rating"
        const val EXTRA_STORE_PROD = "extra_store"
    }

}