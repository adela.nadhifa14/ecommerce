package com.adel.ecommerce.ui.store.wishlist

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.adel.ecommerce.databinding.AdsItem2WishlistBinding
import com.adel.ecommerce.databinding.AdsItemLinearWishlistBinding
import com.adel.ecommerce.service.WishlistProducts
import com.bumptech.glide.Glide
import java.text.NumberFormat
import java.util.Locale

class WishlistAdapter :
    ListAdapter<WishlistProducts, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    var item = true
    private var onItemClickCallback: OnItemClickCallback? = null
    private val wlProducts = ArrayList<WishlistProducts>()

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    interface OnItemClickCallback {
        fun onItemClicked(data: WishlistProducts)
    }

    private var onDeleteItemClickListener: OnDeleteItemClickListener? = null

    interface OnDeleteItemClickListener {
        fun onDeleteItemClicked(productId: String)
    }

    fun setOnDeleteItemClickListener(listener: OnDeleteItemClickListener) {
        onDeleteItemClickListener = listener
    }

    private var onAddToCartItemClickListener: OnAddToCartItemClickListener? = null

    interface OnAddToCartItemClickListener {
        fun onAddToCartItemClicked(data: WishlistProducts)
    }

    fun setOnAddToCartItemClickListener(listener: OnAddToCartItemClickListener) {
        onAddToCartItemClickListener = listener
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setWLItems(wl: ArrayList<WishlistProducts>) {
        wlProducts.clear()
        wlProducts.addAll(wl)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            0 -> LinearLayoutViewHolder(
                AdsItemLinearWishlistBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            1 -> GridLayoutViewHolder(
                AdsItem2WishlistBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            else -> throw IllegalArgumentException("Unknown item type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        when (holder) {
            is LinearLayoutViewHolder -> {
                item?.let {
                    holder.bind(item, onDeleteItemClickListener, onAddToCartItemClickListener)
                }
            }

            is GridLayoutViewHolder -> {
                item?.let {
                    holder.bind(item, onDeleteItemClickListener, onAddToCartItemClickListener)
                }
            }
        }

        holder.itemView.setOnClickListener {
            item?.let { onItemClickCallback?.onItemClicked(it) }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (item) {
            0
        } else 1
    }

    class LinearLayoutViewHolder(private val linearLayout: AdsItemLinearWishlistBinding) :
        RecyclerView.ViewHolder(linearLayout.root) {

        fun bind(
            layout: WishlistProducts,
            onDeleteItemClickListener: OnDeleteItemClickListener?,
            onAddToCartItemClickListener: OnAddToCartItemClickListener?
        ) {
            Glide.with(itemView.context).load(layout.image).into(linearLayout.adsImage)
            linearLayout.adsName.text = layout.productName
            val harga =
                NumberFormat.getNumberInstance(Locale("id","ID")).format(layout.productPrice)
            linearLayout.adsPrice.text =
                StringBuilder().append("Rp").append(harga)
            linearLayout.adsStore.text = layout.store
            linearLayout.adsRate.text = layout.productRating.toString()
            linearLayout.adsSold.text = layout.sale.toString()

            linearLayout.deleteButtonLinear.setOnClickListener {
                layout.productId.let { productId ->
                    onDeleteItemClickListener?.onDeleteItemClicked(productId)
                }
            }

            linearLayout.btnAddToCart.setOnClickListener {
                layout.let { WishlistProducts ->
                    onAddToCartItemClickListener?.onAddToCartItemClicked(WishlistProducts)
                }
            }
        }
    }

    class GridLayoutViewHolder(private val gridLayout: AdsItem2WishlistBinding) :
        RecyclerView.ViewHolder(gridLayout.root) {

        fun bind(
            layout: WishlistProducts,
            onDeleteItemClickListener: OnDeleteItemClickListener?,
            onAddToCartItemClickListener: OnAddToCartItemClickListener?
        ) {
            Glide.with(itemView.context).load(layout.image).into(gridLayout.adsImage)
            gridLayout.adsName.text = layout.productName
            val harga =
                NumberFormat.getNumberInstance(Locale("id","ID")).format(layout.productPrice)
            gridLayout.adsPrice.text =
                StringBuilder().append("Rp").append(harga)
            gridLayout.adsStore.text = layout.store
            gridLayout.adsRate.text = layout.productRating.toString()
            gridLayout.adsSold.text = layout.sale.toString()

            gridLayout.deleteButtonLGrid.setOnClickListener {
                layout.productId.let { productId ->
                    onDeleteItemClickListener?.onDeleteItemClicked(productId)
                }
            }

            gridLayout.btnAddToCart.setOnClickListener {
                layout.let { WishlistProducts ->
                    onAddToCartItemClickListener?.onAddToCartItemClicked(WishlistProducts)
                }
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<WishlistProducts>() {
            override fun areItemsTheSame(
                oldItem: WishlistProducts, newItem: WishlistProducts
            ): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(
                oldItem: WishlistProducts, newItem: WishlistProducts
            ): Boolean {
                return oldItem.productId == newItem.productId
            }
        }
    }
}
