package com.adel.ecommerce.ui.store.notification

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.adel.ecommerce.service.NotificationProducts
import com.adel.ecommerce.service.WishlistProducts
import javax.inject.Singleton

@Database(entities = [NotificationProducts::class], version = 2)
@Singleton
abstract class NotificationDatabase : RoomDatabase() {
    abstract fun notificationDao(): NotificationDAO

    companion object {
        private var INSTANCE: NotificationDatabase? = null

        fun getDatabase(context: Context): NotificationDatabase? {
            if (INSTANCE == null) {
                synchronized(this) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        NotificationDatabase::class.java,
                        "notification database"
                    ).fallbackToDestructiveMigration().build()
                }
            }
            return INSTANCE
        }
    }
}