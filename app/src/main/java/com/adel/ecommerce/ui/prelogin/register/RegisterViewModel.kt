package com.adel.ecommerce.ui.prelogin.register

import androidx.lifecycle.ViewModel
import com.adel.ecommerce.EcommerceRepository
import com.adel.ecommerce.service.Auth
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val repository: EcommerceRepository
) : ViewModel() {

    fun doRegister(auth: Auth) {
        return repository.register(auth)
    }


}