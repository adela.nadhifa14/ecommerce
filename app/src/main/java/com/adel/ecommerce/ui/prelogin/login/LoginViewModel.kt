package com.adel.ecommerce.ui.prelogin.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adel.ecommerce.EcommerceRepository
import com.adel.ecommerce.service.ApiService
import com.adel.ecommerce.service.Auth
import com.adel.ecommerce.service.DataResponse
import com.adel.ecommerce.service.HitApiState
import com.adel.ecommerce.session.SessionPreference
import dagger.hilt.android.lifecycle.HiltViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val apiService: ApiService,
    private val session : SessionPreference
    ) : ViewModel() {

    private val _login = MutableLiveData<DataResponse>()
    val login: LiveData<DataResponse> = _login

    fun login(auth: Auth) {
        apiService.doLogin(auth).enqueue(object : Callback<DataResponse> {
            override fun onResponse(
                call: Call<DataResponse>,
                response: Response<DataResponse>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    session.saveAccessToken(response.body()!!.data!!.accessToken)
                    session.saveRefreshToken(response.body()!!.data!!.refreshToken)
                    session.saveUserImage(response.body()!!.data!!.userImage)
                    session.saveUserName(response.body()!!.data!!.userName)
                    _login.value = response.body()
                }
            }

            override fun onFailure(call: Call<DataResponse>, t: Throwable) {
                Log.d(TAG, "Response : ${t.message.toString()}")
            }
        })
    }

    companion object {
        const val TAG = "LoginViewModel"
    }
}