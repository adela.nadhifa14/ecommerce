package com.adel.ecommerce.ui.store.checkout

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.adel.ecommerce.EcommerceRepository
import com.adel.ecommerce.databinding.ActivityCheckoutBinding
import com.adel.ecommerce.service.Payment
import com.adel.ecommerce.service.PaymentItem
import com.adel.ecommerce.service.WishlistProducts
import com.adel.ecommerce.ui.store.cart.CartViewModel
import com.adel.ecommerce.ui.store.notification.MyFirebaseMessagingService
import com.bumptech.glide.Glide
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.AndroidEntryPoint
import java.text.NumberFormat
import java.util.Locale
import javax.inject.Inject

@AndroidEntryPoint
class CheckoutActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCheckoutBinding
    private lateinit var adapter: CheckoutAdapter
    private val viewModel: CartViewModel by viewModels()

    private var paymentMethodName: String? = null
    private var paymentMethodImage: String? = null

    private var productId: String? = null
    private var variantName: String? = null
    private var productName: String? = null
    private var quantity: Int? = 0
    private var productPrice: Int? = 0
    private var variantPrice: Int? = 0

    @Inject
    lateinit var repository: EcommerceRepository

    @Inject
    lateinit var analytics: FirebaseAnalytics

    @Inject
    lateinit var messaging: FirebaseMessaging

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCheckoutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val sourceActivity = intent.getStringExtra(EXTRA_SOURCE_ACTIVITY)

        val list = intent.getParcelableArrayListExtra<WishlistProducts>(EXTRA_PROD)

        if (list != null) {
            for (item in list) {
                productId = item.productId
                variantName = item.variantName
                variantPrice = item.variantPrice
                quantity = item.quantity
                productPrice = item.productPrice
                productName = item.productName
            }
        }

        paymentMethodName = intent.getStringExtra(EXTRA_PAYMENT_NAME)
        paymentMethodImage = intent.getStringExtra(EXTRA_PAYMENT_IMAGE)

        if (sourceActivity == "CartActivity") {
            binding.apply {
                adapter = CheckoutAdapter(viewModel)

                rvCheckout.adapter = adapter
                rvCheckout.layoutManager = GridLayoutManager(this@CheckoutActivity, 1)

                list?.let {
                    adapter.setCheckoutItem(it)
                    adapter.submitList(it)

                    val beginCheckout = Bundle()
                    beginCheckout.putString(FirebaseAnalytics.Param.ITEM_ID, productId)
                    beginCheckout.putString(FirebaseAnalytics.Param.ITEM_NAME, productName)
                    beginCheckout.putString(
                        FirebaseAnalytics.Param.VALUE,
                        (quantity!! * productPrice!!).toString()
                    )
                    beginCheckout.putString(FirebaseAnalytics.Param.CURRENCY, "IDR")
                    val params = Bundle()
                    params.putParcelableArray(FirebaseAnalytics.Param.ITEMS, arrayOf(beginCheckout))

                    analytics.logEvent(FirebaseAnalytics.Event.BEGIN_CHECKOUT, params)

                }

                viewModel.getCartItem()?.observe(this@CheckoutActivity) { it ->
                    if (it != null) {
                        val totalPrice = it.filter {
                            it.selected
                        }.sumOf { it.productPrice * it.quantity }

                        val harga =
                            NumberFormat.getNumberInstance(Locale("id","ID")).format(totalPrice)

                        tvHargaCheckout.text = StringBuilder().append("Rp").append(harga)
                    }
                }
            }
        } else if (sourceActivity == "DetailProductActivity") {
            binding.apply {
                adapter = CheckoutAdapter(viewModel)

                rvCheckout.adapter = adapter
                rvCheckout.layoutManager = GridLayoutManager(this@CheckoutActivity, 1)

                list?.let {
                    adapter.setCheckoutItem(it)
                    adapter.submitList(it)

                    val harga = NumberFormat.getNumberInstance(Locale("id","ID"))
                        .format(productPrice!! + variantPrice!!)
                    tvHargaCheckout.text = StringBuilder().append("Rp").append(harga)

                    adapter.setOnQuantityChangeListener(object :
                        CheckoutAdapter.OnQuantityChangeListener {
                        override fun onQuantityChanged(productId: String, newQuantity: Int) {
                            val totalPrice = it.filter { it.selected }
                                .sumOf { it.productPrice * newQuantity }
                            val harga = NumberFormat.getNumberInstance(Locale("id","ID"))
                                .format(totalPrice + variantPrice!!)
                            tvHargaCheckout.text = StringBuilder().append("Rp").append(harga)
                        }
                    })

                }
            }
        }

        binding.btnPilihPembayaran.setOnClickListener {
            val intentToMetodePembayaran =
                Intent(this@CheckoutActivity, PaymentMethodActivity::class.java)
            startActivityForResult(intentToMetodePembayaran, REQUEST_PAYMENT_METHOD)
        }

        binding.btnBayarCart.setOnClickListener {
            if (paymentMethodName != null) {
                repository.payment(
                    Payment(
                        paymentMethodName.toString(), listOf(
                            PaymentItem(
                                productId.toString(), variantName.toString(),
                                quantity!!
                            )
                        )
                    )
                )

                repository.payment.observe(this@CheckoutActivity) {
                    val intentToResponseFulfillment =
                        Intent(this@CheckoutActivity, ResponseFulfillmentActivity::class.java)
                    intentToResponseFulfillment.putExtra(
                        ResponseFulfillmentActivity.EXTRA_DATA_RESPONSE,
                        it
                    )
                    intentToResponseFulfillment.putExtra(
                        ResponseFulfillmentActivity.EXTRA_SOURCE_ACTIVITY,
                        "CheckoutActivity"
                    )
                    startActivity(intentToResponseFulfillment)


                    finish()
                }
            } else {
                Toast.makeText(this, "Select payment method", Toast.LENGTH_SHORT).show()
            }


        }

        binding.topAppBar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_PAYMENT_METHOD && resultCode == RESULT_OK) {
            paymentMethodName = data?.getStringExtra(EXTRA_PAYMENT_NAME)
            paymentMethodImage = data?.getStringExtra(EXTRA_PAYMENT_IMAGE)

            binding.bankName.text = paymentMethodName
            Glide.with(this@CheckoutActivity)
                .load(paymentMethodImage)
                .into(binding.bankImage)

            Log.d("onActivityResult", "onActivityResult name: $paymentMethodName")

            val addPaymentInfo = Bundle()
            addPaymentInfo.putString(FirebaseAnalytics.Param.PAYMENT_TYPE, paymentMethodName)

            analytics.logEvent(FirebaseAnalytics.Event.ADD_PAYMENT_INFO, addPaymentInfo)
        }
    }

    companion object {
        const val EXTRA_PROD = "extra_product"
        const val EXTRA_PAYMENT_NAME = "extra_payment_name"
        const val EXTRA_PAYMENT_IMAGE = "extra_payment_image"
        private const val REQUEST_PAYMENT_METHOD = 1
        const val EXTRA_SOURCE_ACTIVITY = "extra_source_activity"
    }
}