package com.adel.ecommerce.ui.store.pagingStore

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.adel.ecommerce.databinding.AdsItem2Binding
import com.adel.ecommerce.databinding.AdsItemLinearBinding
import com.adel.ecommerce.service.GetProductsItemResponse
import com.bumptech.glide.Glide
import java.text.NumberFormat
import java.util.Locale

class ProductPagingAdapter :
    PagingDataAdapter<GetProductsItemResponse, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    var item = true
    private var onItemClickCallback: OnItemClickCallback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            0 -> LinearLayoutViewHolder(
                AdsItemLinearBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            1 -> GridLayoutViewHolder(
                AdsItem2Binding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            else -> throw IllegalArgumentException("Unknown item type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        when (holder) {
            is LinearLayoutViewHolder -> {
                item?.let {
                    holder.bind(item)
                }
            }

            is GridLayoutViewHolder -> {
                item?.let {
                    holder.bind(item)
                }
            }
        }

        holder.itemView.setOnClickListener {
            item?.let { onItemClickCallback?.onItemClicked(it) }
        }
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    interface OnItemClickCallback {
        fun onItemClicked(data: GetProductsItemResponse)
    }

    override fun getItemViewType(position: Int): Int {
        return if (item) {
            0
        } else 1
    }

    class LinearLayoutViewHolder(private val linearLayout: AdsItemLinearBinding) :
        RecyclerView.ViewHolder(linearLayout.root) {

        fun bind(layout: GetProductsItemResponse) {
            Glide.with(itemView.context).load(layout.image).into(linearLayout.adsImage)
            linearLayout.adsName.text = layout.productName
            val harga =
                NumberFormat.getNumberInstance(Locale("id","ID")).format(layout.productPrice)
            linearLayout.adsPrice.text =
                StringBuilder().append("Rp").append(harga)
            linearLayout.adsStore.text = layout.store
            linearLayout.adsRate.text = layout.productRating.toString()
            linearLayout.adsSold.text = layout.sale.toString()
        }
    }

    class GridLayoutViewHolder(private val gridLayout: AdsItem2Binding) :
        RecyclerView.ViewHolder(gridLayout.root) {

        fun bind(layout: GetProductsItemResponse) {
            Glide.with(itemView.context).load(layout.image).into(gridLayout.adsImage)
            gridLayout.adsName.text = layout.productName
            val harga =
                NumberFormat.getNumberInstance(Locale("id","ID")).format(layout.productPrice)
            gridLayout.adsPrice.text =
                StringBuilder().append("Rp").append(harga)
            gridLayout.adsStore.text = layout.store
            gridLayout.adsRate.text = layout.productRating.toString()
            gridLayout.adsSold.text = layout.sale.toString()
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<GetProductsItemResponse>() {
            override fun areItemsTheSame(
                oldItem: GetProductsItemResponse, newItem: GetProductsItemResponse
            ): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(
                oldItem: GetProductsItemResponse, newItem: GetProductsItemResponse
            ): Boolean {
                return oldItem.productId == newItem.productId
            }
        }
    }
}
