package com.adel.ecommerce.ui.prelogin.register

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doOnTextChanged
import com.adel.ecommerce.EcommerceRepository
import com.adel.ecommerce.R
import com.adel.ecommerce.databinding.ActivityRegisterBinding
import com.adel.ecommerce.service.Auth
import com.adel.ecommerce.session.SessionPreference
import com.adel.ecommerce.ui.MainActivity
import com.adel.ecommerce.ui.prelogin.login.LoginActivity
import com.adel.ecommerce.ui.prelogin.profile.ProfileActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class RegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterBinding
    private val viewModel: RegisterViewModel by viewModels()

    @Inject
    lateinit var analytics: FirebaseAnalytics

    @Inject
    lateinit var messaging: FirebaseMessaging

    @Inject
    lateinit var repository: EcommerceRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setupAction()
        toLogin()
        syaratKetentuan()
    }

    private fun setMyButtonEnable() {
        binding.apply {
            val emailSet = emailEditText.text
            val passwordSet = passwordEditText.text

            registerBtn.isEnabled = emailSet != null && passwordSet != null
                    && emailSet.toString().isNotEmpty() && passwordSet.toString()
                .isNotEmpty() && passwordSet.length >= 8 && Patterns.EMAIL_ADDRESS.matcher(
                emailSet.toString()
            ).matches()

        }
    }

    private fun setupAction() {
        binding.apply {

            emailEditText.addTextChangedListener {
                setMyButtonEnable()
            }

            passwordEditText.addTextChangedListener {
                setMyButtonEnable()
            }

            emailEditText.doOnTextChanged { text, _, _, _ ->
                if (text.toString().isEmpty()) {
                    emailETL.error = null
                    emailETL.isErrorEnabled = false
                } else if (!Patterns.EMAIL_ADDRESS.matcher(text.toString()).matches()) {
                    emailETL.error = getString(R.string.email_error)
                } else {
                    emailETL.error = null
                    emailETL.isErrorEnabled = false
                }
            }

            passwordEditText.doOnTextChanged { text, _, _, _ ->
                if (text.toString().isEmpty()) {
                    passwordEditTextLayout.error = null
                    passwordEditTextLayout.isErrorEnabled = false
                } else if (text!!.length < 8) {
                    passwordEditTextLayout.error = getString(R.string.password_error)
                } else {
                    passwordEditTextLayout.error = null
                    passwordEditTextLayout.isErrorEnabled = false
                }
            }

            registerBtn.setOnClickListener {
                val email = emailEditText.text.toString()
                val password = passwordEditText.text.toString()

                messaging.token.addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Log.w("Register", "Fetching FCM registration token failed", task.exception)
                        return@OnCompleteListener
                    }

                    val token = task.result
                    showLoading(true)
                    viewModel.doRegister(Auth(email, password, token))

                    Log.d("Register", "firebase : $token")
                })

                showLoading(false)

                val intent = Intent(this@RegisterActivity, ProfileActivity::class.java)
                startActivity(intent)
                finish()

                val bundle = Bundle()
                val signup = "adel"
                bundle.putString(FirebaseAnalytics.Param.METHOD, signup)

                analytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle)

            }
        }
    }

    private fun toLogin() {
        binding.loginBtn.setOnClickListener {
            val intentToLogin = Intent(this, LoginActivity::class.java)
            startActivity(intentToLogin)
        }
    }

    private fun syaratKetentuan() {
        binding.apply {
            val text = resources.getString(R.string.terms_and_conditions)
            val spannableString = SpannableStringBuilder(text)

            val colorSpan1 = ForegroundColorSpan(Color.parseColor("#A0A0A0"))
            spannableString.setSpan(colorSpan1, 0, 37, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

            val colorSpan2 = ForegroundColorSpan(Color.parseColor("#6750A4"))
            spannableString.setSpan(colorSpan2, 37, 55, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

            val colorSpan3 = ForegroundColorSpan(Color.parseColor("#A0A0A0"))
            spannableString.setSpan(colorSpan3, 56, 62, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

            val colorSpan4 = ForegroundColorSpan(Color.parseColor("#6750A4"))
            spannableString.setSpan(colorSpan4, 63, 80, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

            val colorSpan5 = ForegroundColorSpan(Color.parseColor("#A0A0A0"))
            spannableString.setSpan(colorSpan5, 81, 92, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

            syaratKetentuan.text = spannableString
        }
    }

    private fun showLoading(isLoading: Boolean) {
        binding.progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}