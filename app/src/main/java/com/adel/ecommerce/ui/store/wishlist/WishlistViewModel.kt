package com.adel.ecommerce.ui.store.wishlist

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.adel.ecommerce.service.WishlistProducts
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WishlistViewModel @Inject constructor(
    application: Application
) : ViewModel() {

    private var wishlistDb: WishlistDatabase? = WishlistDatabase.getDatabase(application)
    private var wishlistDao = wishlistDb?.wishlistItemDao()

    fun addToWishlist(
        productId: String,
        productName: String,
        productPrice: Int,
        image: String,
        store: String,
        sale: Int,
        stock: Int?,
        productRating: Float,
        variantName: String?,
        variantPrice: Int?
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            val wlItem = WishlistProducts(
                productId,
                productName,
                productPrice,
                image,
                "",
                "",
                store,
                sale,
                stock,
                0,
                0,
                0,
                productRating,
                variantName ?: "RAM 16",
                variantPrice ?: 0
            )
            wishlistDao?.addToWL(wlItem)
        }
    }


    suspend fun checkWl(id: String) = wishlistDao?.checkWLProducts(id)

    fun removeFromWL(id: String) {
        CoroutineScope(Dispatchers.IO).launch {
            wishlistDao?.removeFromWL(id)
        }
    }

    fun getWL(): LiveData<List<WishlistProducts>>? {
        return wishlistDao?.getWlProducts()
    }

}