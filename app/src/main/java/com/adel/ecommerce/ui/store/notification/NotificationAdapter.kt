package com.adel.ecommerce.ui.store.notification

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.adel.ecommerce.databinding.NotifItemBinding
import com.adel.ecommerce.service.NotificationProducts
import com.bumptech.glide.Glide
import javax.inject.Inject

class NotificationAdapter @Inject constructor(private val viewModel: NotificationViewModel) :
    ListAdapter<NotificationProducts, NotificationAdapter.NotificationViewHolder>(DIFF_CALLBACK) {

    private val notifItem = ArrayList<NotificationProducts>()

    fun setNotifItem(users: ArrayList<NotificationProducts>) {
        notifItem.clear()
        notifItem.addAll(users)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val binding =
            NotifItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NotificationViewHolder(binding)
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        val notifItems = notifItem[position]

        holder.binding.tvInfo.text = notifItems.type
        Glide.with(holder.itemView.context)
            .load(notifItems.image)
            .into(holder.binding.imageNotif)
        holder.binding.date.text = notifItems.date
        holder.binding.tvBody.text = notifItems.body

        if (!notifItems.isRead) {
            holder.itemView.setBackgroundColor(Color.parseColor("#EADDFF"))
        }

        holder.itemView.setOnClickListener {
            viewModel.isReadNotification(true, notifItems.id)
        }

    }


    class NotificationViewHolder(var binding: NotifItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<NotificationProducts>() {
            override fun areItemsTheSame(
                oldItem: NotificationProducts, newItem: NotificationProducts
            ): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(
                oldItem: NotificationProducts, newItem: NotificationProducts
            ): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }

}