package com.adel.ecommerce.ui.store.transaksi

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.adel.ecommerce.EcommerceRepository
import com.adel.ecommerce.R
import com.adel.ecommerce.databinding.ActivityTransactionBinding
import com.adel.ecommerce.service.PaymentDataResponse
import com.adel.ecommerce.service.PaymentItem
import com.adel.ecommerce.service.TransactionDataResponse
import com.adel.ecommerce.session.SessionPreference
import com.adel.ecommerce.ui.MainActivity
import com.adel.ecommerce.ui.prelogin.login.LoginActivity
import com.adel.ecommerce.ui.store.StoreActivity
import com.adel.ecommerce.ui.store.cart.CartActivity
import com.adel.ecommerce.ui.store.cart.CartViewModel
import com.adel.ecommerce.ui.store.checkout.CheckoutActivity
import com.adel.ecommerce.ui.store.checkout.ResponseFulfillmentActivity
import com.adel.ecommerce.ui.store.detailProduct.DetailProductActivity
import com.adel.ecommerce.ui.store.notification.NotificationActivity
import com.adel.ecommerce.ui.store.wishlist.WishlistActivity
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.google.android.material.badge.ExperimentalBadgeUtils
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class TransactionActivity : AppCompatActivity() {
    private lateinit var binding: ActivityTransactionBinding
    private lateinit var adapter: TransactionAdapter
    private val viewModel: CartViewModel by viewModels()

    @Inject
    lateinit var repository: EcommerceRepository

    @ExperimentalBadgeUtils
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityTransactionBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setupBottomNav()
        setupToolbar()

        viewModel.getCartItem()?.observe(this) {
            binding.topAppBar.viewTreeObserver.addOnGlobalLayoutListener {
                val badgeDrawable = BadgeDrawable.create(this).apply {
                    isVisible = it.isNotEmpty()
                    number = it.size
                }
                BadgeUtils.attachBadgeDrawable(
                    badgeDrawable,
                    binding.topAppBar,
                    R.id.toolbar_cart
                )
            }
        }

        adapter = TransactionAdapter()
        binding.rvTransaction.adapter = adapter

        binding.apply {
            repository.getTransaction()
            repository.transaction.observe(this@TransactionActivity) { transaction ->
                adapter.setTransactionItem(ArrayList(transaction))
                Log.d("Transaction", "transaction items : $transaction")
                rvTransaction.layoutManager = GridLayoutManager(this@TransactionActivity, 1)

                if (transaction.isEmpty()) {
                    binding.rvTransaction.visibility = View.GONE
                    binding.errorLayout.errorLayout.visibility = View.VISIBLE
                    binding.errorLayout.errorTypeText.text = "Empty"
                    binding.errorLayout.errorTypeInfo.text = "Your requested data is unavailable"
                    binding.errorLayout.restartButton.visibility = View.GONE
                }

                adapter.setOnItemClickListener(object : TransactionAdapter.OnItemClickListener {
                    override fun onItemClicked(isReview: TransactionDataResponse) {
                        val intentToReview = Intent(
                            this@TransactionActivity,
                            ResponseFulfillmentActivity::class.java
                        )
                        intentToReview.putExtra(ResponseFulfillmentActivity.EXTRA_DATA_RESPONSE, isReview)
                        intentToReview.putExtra(ResponseFulfillmentActivity.EXTRA_SOURCE_ACTIVITY, "TransactionActivity")
                        startActivity(intentToReview)
                    }
                })
            }
        }
    }

    private fun setupBottomNav() {
        val bottomNav = binding.navView

        bottomNav.selectedItemId = R.id.navigation_transaksi

        bottomNav.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_transaksi -> {
                    return@setOnItemSelectedListener true
                }

                R.id.navigation_store -> {
                    startActivity(Intent(applicationContext, StoreActivity::class.java))
                    overridePendingTransition(0, 0)
                    return@setOnItemSelectedListener true
                }

                R.id.navigation_home -> {
                    startActivity(Intent(applicationContext, MainActivity::class.java))
                    overridePendingTransition(0, 0)
                    return@setOnItemSelectedListener true
                }

                R.id.navigation_wishlist -> {
                    startActivity(Intent(applicationContext, WishlistActivity::class.java))
                    overridePendingTransition(0, 0)
                    return@setOnItemSelectedListener true
                }
            }
            return@setOnItemSelectedListener false
        }
    }

    private fun setupToolbar() {
        binding.apply {
            val sessionPreference = SessionPreference(this@TransactionActivity)
            topAppBar.title = sessionPreference.passUserName().toString()

            topAppBar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.toolbar_notifications -> {
                        val intentToNotifs =
                            Intent(this@TransactionActivity, NotificationActivity::class.java)
                        startActivity(intentToNotifs)
                        true
                    }

                    R.id.toolbar_cart -> {
                        val intentToCart =
                            Intent(this@TransactionActivity, CartActivity::class.java)
                        startActivity(intentToCart)
                        true
                    }

                    R.id.toolbar_menu -> {
                        true
                    }

                    else -> false
                }
            }
        }
    }
}