package com.adel.ecommerce.ui.store.wishlist

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.adel.ecommerce.R
import com.adel.ecommerce.databinding.ActivityWishlistBinding
import com.adel.ecommerce.service.WishlistProducts
import com.adel.ecommerce.session.SessionPreference
import com.adel.ecommerce.ui.MainActivity
import com.adel.ecommerce.ui.prelogin.login.LoginActivity
import com.adel.ecommerce.ui.store.StoreActivity
import com.adel.ecommerce.ui.store.cart.CartActivity
import com.adel.ecommerce.ui.store.cart.CartViewModel
import com.adel.ecommerce.ui.store.detailProduct.DetailProductActivity
import com.adel.ecommerce.ui.store.notification.NotificationActivity
import com.adel.ecommerce.ui.store.transaksi.TransactionActivity
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.google.android.material.badge.ExperimentalBadgeUtils
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WishlistActivity :
    AppCompatActivity() {
    private lateinit var binding: ActivityWishlistBinding
    private lateinit var adapter: WishlistAdapter
    private val viewModel: WishlistViewModel by viewModels()
    private val cartViewModel: CartViewModel by viewModels()

    private lateinit var rootView: View

    @ExperimentalBadgeUtils
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityWishlistBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        rootView = findViewById(android.R.id.content)

        setupToolbar()
        setupBottomNav()

        cartViewModel.getCartItem()?.observe(this) {
            binding.topAppBar.viewTreeObserver.addOnGlobalLayoutListener {
                val badgeDrawable = BadgeDrawable.create(this).apply {
                    isVisible = it.isNotEmpty()
                    number = it.size
                }
                BadgeUtils.attachBadgeDrawable(
                    badgeDrawable,
                    binding.topAppBar,
                    R.id.toolbar_cart
                )
            }
        }

        adapter = WishlistAdapter()
        binding.rvWishlist.layoutManager = GridLayoutManager(this, 1)
        binding.rvWishlist.adapter = adapter

        viewModel.getWL()?.observe(this) {
            if (it != null) {
                val productList: List<WishlistProducts> = it

                val arrayList: ArrayList<WishlistProducts> = ArrayList(productList)

                adapter.setWLItems(arrayList)

                binding.nBarang.text = it.size.toString()
            }

            if (it.isNullOrEmpty()) {
                binding.rvWishlist.visibility = View.GONE
                binding.errorLayout.errorLayout.visibility = View.VISIBLE
                binding.errorLayout.errorTypeText.text = "Empty"
                binding.errorLayout.errorTypeInfo.text = "Your requested data is unavailable"
                binding.errorLayout.restartButton.visibility = View.GONE
            } else {
                binding.rvWishlist.visibility = View.VISIBLE
                binding.errorLayout.errorLayout.visibility = View.GONE
            }
        }

        viewModel.getWL()?.observe(this) {
            if (it != null) {
                adapter.submitList(it)

                binding.convertButton.setOnClickListener {
                    adapter.item = !adapter.item
                    if (adapter.item) {
                        binding.convertButton.setImageResource(R.drawable.baseline_list_alt_24)
                        binding.rvWishlist.layoutManager = GridLayoutManager(this, 1)
                    } else {
                        binding.convertButton.setImageResource(R.drawable.baseline_grid_view_24)
                        binding.rvWishlist.layoutManager = GridLayoutManager(this, 2)
                    }
                }
            }
        }

        adapter.setOnItemClickCallback(object : WishlistAdapter.OnItemClickCallback {
            override fun onItemClicked(data: WishlistProducts) {
                val intentToDetail =
                    Intent(this@WishlistActivity, DetailProductActivity::class.java)
                intentToDetail.putExtra(DetailProductActivity.EXTRA_ID_DETAIL, data.productId)
                intentToDetail.putExtra(DetailProductActivity.EXTRA_NAME_PROD, data.productName)
                intentToDetail.putExtra(DetailProductActivity.EXTRA_PRICE_PROD, data.productPrice)
                intentToDetail.putExtra(DetailProductActivity.EXTRA_IMAGE_PROD, data.image)
                intentToDetail.putExtra(DetailProductActivity.EXTRA_SALE_PROD, data.sale)
                intentToDetail.putExtra(DetailProductActivity.EXTRA_STORE_PROD, data.store)
                intentToDetail.putExtra(DetailProductActivity.EXTRA_RATING_PROD, data.productRating)

                startActivity(intentToDetail)
            }
        })

        adapter.setOnDeleteItemClickListener(object : WishlistAdapter.OnDeleteItemClickListener {
            override fun onDeleteItemClicked(productId: String) {
                viewModel.removeFromWL(productId)
                Snackbar.make(rootView, "Produk dihapus dari wishlist", Snackbar.LENGTH_SHORT)
                    .show()
            }
        })

        viewModel.getWL()?.observe(this) {
            if (it != null) {
                adapter.setOnAddToCartItemClickListener(object :
                    WishlistAdapter.OnAddToCartItemClickListener {
                    override fun onAddToCartItemClicked(data: WishlistProducts) {
                        cartViewModel.addToCart(
                            data.productId,
                            data.productName,
                            data.productPrice,
                            data.image,
                            data.stock,
                            "RAM 16",
                            0
                        )
                        Snackbar.make(
                            rootView,
                            "Produk berhasil ditambahkan ke keranjang!",
                            Snackbar.LENGTH_SHORT
                        ).show()

                    }
                })
            }
        }
    }

    private fun setupBottomNav() {
        val bottomNav = binding.navView

        bottomNav.selectedItemId = R.id.navigation_wishlist

        bottomNav.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_wishlist -> {
                    return@setOnItemSelectedListener true
                }

                R.id.navigation_store -> {
                    startActivity(Intent(applicationContext, StoreActivity::class.java))
                    overridePendingTransition(0, 0)
                    return@setOnItemSelectedListener true
                }

                R.id.navigation_home -> {
                    startActivity(Intent(applicationContext, MainActivity::class.java))
                    overridePendingTransition(0, 0)
                    return@setOnItemSelectedListener true
                }

                R.id.navigation_transaksi -> {
                    startActivity(Intent(applicationContext, TransactionActivity::class.java))
                    overridePendingTransition(0, 0)
                    return@setOnItemSelectedListener true
                }
            }
            return@setOnItemSelectedListener false
        }
    }

    private fun setupToolbar() {
        binding.apply {
            val sessionPreference = SessionPreference(this@WishlistActivity)
            topAppBar.title = sessionPreference.passUserName().toString()

            topAppBar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.toolbar_notifications -> {
                        val intentToNotifs =
                            Intent(this@WishlistActivity, NotificationActivity::class.java)
                        startActivity(intentToNotifs)
                        true
                    }

                    R.id.toolbar_cart -> {
                        val intentToCart = Intent(this@WishlistActivity, CartActivity::class.java)
                        startActivity(intentToCart)
                        true
                    }

                    R.id.toolbar_menu -> {
                        true
                    }

                    else -> false
                }
            }
        }
    }

}