package com.adel.ecommerce.ui.store.wishlist

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.adel.ecommerce.service.WishlistProducts

@Dao
interface WishlistDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addToWL(wlProducts: WishlistProducts): Long

    @Query("SELECT * FROM wishlist_products")
    fun getWlProducts(): LiveData<List<WishlistProducts>>

    @Query("SELECT count(*) FROM wishlist_products WHERE wishlist_products.productId = :id")
    suspend fun checkWLProducts(id: String): Int

    @Query("DELETE FROM wishlist_products WHERE wishlist_products.productId = :id")
    suspend fun removeFromWL(id: String): Int
}