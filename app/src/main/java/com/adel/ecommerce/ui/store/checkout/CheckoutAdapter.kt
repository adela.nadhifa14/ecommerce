package com.adel.ecommerce.ui.store.checkout

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.adel.ecommerce.R
import com.adel.ecommerce.databinding.CheckoutProductItemBinding
import com.adel.ecommerce.service.WishlistProducts
import com.adel.ecommerce.ui.store.cart.CartViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import java.text.NumberFormat
import java.util.Locale
import javax.inject.Inject

class CheckoutAdapter @Inject constructor(private val viewModel: CartViewModel) :
    ListAdapter<WishlistProducts, CheckoutAdapter.CheckoutViewHolder>(DIFF_CALLBACK) {

    private val checkOutItem = ArrayList<WishlistProducts>()

    fun setCheckoutItem(users: ArrayList<WishlistProducts>) {
        checkOutItem.clear()
        checkOutItem.addAll(users)
    }

    private var onQuantityChangeListener: OnQuantityChangeListener? = null

    interface OnQuantityChangeListener {
        fun onQuantityChanged(productId: String, newQuantity: Int)
    }

    fun setOnQuantityChangeListener(listener: OnQuantityChangeListener) {
        this.onQuantityChangeListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheckoutViewHolder {
        val binding =
            CheckoutProductItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CheckoutViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CheckoutViewHolder, position: Int) {
        val checkOutItem = checkOutItem[position]

        holder.binding.let {
            checkOutItem.let { checkOutItem ->
                it.adsName.text = checkOutItem.productName
                val harga =
                    NumberFormat.getNumberInstance(Locale("id","ID"))
                        .format((checkOutItem.productPrice) + checkOutItem.variantPrice!!)
                it.adsPrice.text = StringBuilder().append("Rp").append(harga)
                Glide.with(holder.itemView.context)
                    .load(checkOutItem.image)
                    .centerCrop()
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(it.adsImage)
                it.tvSisaProduk.text =
                    StringBuilder().append("Sisa ").append(checkOutItem.stock)
                it.tvVarian.text = checkOutItem.variantName

                val textCounterButton =
                    it.include.buttonCounter.findViewById<Button>(R.id.text_counter)
                textCounterButton.text = checkOutItem.quantity.toString()

                it.include.buttonMinus.setOnClickListener { btn_minus ->
                    if (checkOutItem.quantity > 1) {
                        checkOutItem.quantity -= 1
                        textCounterButton.text = checkOutItem.quantity.toString()
                    } else if (checkOutItem.quantity == 1) {
                        textCounterButton.text = checkOutItem.quantity.toString()
                    }
                    onQuantityChangeListener?.onQuantityChanged(
                        checkOutItem.productId,
                        checkOutItem.quantity
                    )
                    viewModel.updateCartItemQuantity(checkOutItem.productId, checkOutItem.quantity)
                }

                it.include.buttonPlus.setOnClickListener { btn_plus ->
                    if (checkOutItem.quantity < checkOutItem.stock!!) {
                        checkOutItem.quantity += 1
                        textCounterButton.text = checkOutItem.quantity.toString()
                    }
                    onQuantityChangeListener?.onQuantityChanged(
                        checkOutItem.productId,
                        checkOutItem.quantity
                    )
                    viewModel.updateCartItemQuantity(checkOutItem.productId, checkOutItem.quantity)
                }
            }
        }
    }


    class CheckoutViewHolder(var binding: CheckoutProductItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<WishlistProducts>() {
            override fun areItemsTheSame(
                oldItem: WishlistProducts, newItem: WishlistProducts
            ): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(
                oldItem: WishlistProducts, newItem: WishlistProducts
            ): Boolean {
                return oldItem.productId == newItem.productId
            }
        }
    }

}