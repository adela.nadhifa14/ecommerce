package com.adel.ecommerce.ui.store.pagingStore

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.adel.ecommerce.EcommerceRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PagingProductsViewModel @Inject constructor(
    private val ecommerceRepository: EcommerceRepository
) :
    ViewModel() {

    fun allProducts(
        search: String? = null,
        brand: String? = null,
        lowest: Int? = null,
        highest: Int? = null,
        sort: String? = null
    ) =
        ecommerceRepository.getProductPaging(search, brand, lowest, highest, sort)
            .cachedIn(viewModelScope)
}
