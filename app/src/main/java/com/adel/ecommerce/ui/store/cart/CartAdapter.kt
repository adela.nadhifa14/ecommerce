package com.adel.ecommerce.ui.store.cart

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.adel.ecommerce.R
import com.adel.ecommerce.databinding.CartProductItemBinding
import com.adel.ecommerce.service.WishlistProducts
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import java.text.NumberFormat
import java.util.Locale
import javax.inject.Inject

class CartAdapter @Inject constructor(private val viewModel: CartViewModel) :
    ListAdapter<WishlistProducts, CartAdapter.CartViewHolder>(DIFF_CALLBACK) {

    private val cartItem = ArrayList<WishlistProducts>()

    fun setCartItem(users: ArrayList<WishlistProducts>) {
        cartItem.clear()
        cartItem.addAll(users)
    }

    private var onDeleteItemClickListener: OnDeleteItemClickListener? = null

    interface OnDeleteItemClickListener {
        fun onDeleteItemClicked(productId: String)
    }

    fun setOnDeleteItemClickListener(listener: OnDeleteItemClickListener) {
        onDeleteItemClickListener = listener
    }

    private var onDeleteItemCheckedClickListener: OnDeleteItemCheckedClickListener? = null

    interface OnDeleteItemCheckedClickListener {
        fun onDeleteItemCheckedClicked(isChecked: Boolean)
    }

    fun setOnDeleteItemCheckedClickListener(listener: OnDeleteItemCheckedClickListener) {
        onDeleteItemCheckedClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val binding =
            CartProductItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CartViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        val (productId, productName, productPrize, image, _, _, _, _, stock, _, _, _, _, variantName, _) = cartItem[position]
        val cartItems = cartItem[position]

        holder.binding.adsName.text = productName
        val harga =
            NumberFormat.getNumberInstance(Locale("id", "ID")).format(productPrize)
        holder.binding.adsPrice.text = StringBuilder().append("Rp").append(harga)
        Glide.with(holder.itemView.context)
            .load(image)
            .centerCrop()
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(holder.binding.adsImage)
        holder.binding.tvSisaProduk.text = StringBuilder().append("Sisa ").append(stock)
        holder.binding.tvVarian.text = variantName

        if (cartItems.stock!! < 10) {
            holder.binding.tvSisaProduk.setTextColor(Color.parseColor("#B3261E"))
        }

        holder.binding.deleteButton.setOnClickListener {
            productId.let { productId ->
                onDeleteItemClickListener?.onDeleteItemClicked(productId)
            }
        }

        val textCounterButton =
            holder.binding.include.buttonCounter.findViewById<Button>(R.id.text_counter)
        textCounterButton.text = cartItems.quantity.toString()

        var isAnyItemChecked = false

        if (cartItems.selected) {
            isAnyItemChecked = true
        }

        onDeleteItemCheckedClickListener?.onDeleteItemCheckedClicked(isAnyItemChecked)

        holder.binding.checkBox2.isChecked = cartItems.selected

        holder.binding.checkBox2.setOnCheckedChangeListener { checkbox, isChecked ->
            if (checkbox.isPressed) {
                viewModel.updateCartItemCheckbox(listOf(cartItems.productId), isChecked)
            }

            isAnyItemChecked = isChecked || (isAnyItemChecked && !checkbox.isPressed)

            onDeleteItemCheckedClickListener?.onDeleteItemCheckedClicked(isAnyItemChecked)
        }



        holder.binding.include.buttonMinus.setOnClickListener {
            if (cartItems.quantity > 1) {
                cartItems.quantity -= 1
                textCounterButton.text = cartItems.quantity.toString()
            } else if (cartItems.quantity == 1) {
                textCounterButton.text = cartItems.quantity.toString()
            }
            viewModel.updateCartItemQuantity(cartItems.productId, cartItems.quantity)
        }

        holder.binding.include.buttonPlus.setOnClickListener {
            if (cartItems.quantity < stock!!) {
                cartItems.quantity += 1
                textCounterButton.text = cartItems.quantity.toString()
            }
            viewModel.updateCartItemQuantity(cartItems.productId, cartItems.quantity)
        }

    }


    class CartViewHolder(var binding: CartProductItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<WishlistProducts>() {
            override fun areItemsTheSame(
                oldItem: WishlistProducts, newItem: WishlistProducts
            ): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(
                oldItem: WishlistProducts, newItem: WishlistProducts
            ): Boolean {
                return oldItem.productId == newItem.productId
            }
        }
    }

}