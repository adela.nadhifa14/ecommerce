package com.adel.ecommerce.ui.store.detailProduct

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.adel.ecommerce.databinding.ReviewProductItemBinding
import com.adel.ecommerce.service.ReviewBuyerDataResponse
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions


class ReviewProductAdapter : RecyclerView.Adapter<ReviewProductAdapter.ReviewBuyerViewHolder>() {

    private val reviewBuyer = ArrayList<ReviewBuyerDataResponse>()

    @SuppressLint("NotifyDataSetChanged")
    fun setReviewBuyer(users: ArrayList<ReviewBuyerDataResponse>) {
        reviewBuyer.clear()
        reviewBuyer.addAll(users)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewBuyerViewHolder {
        val binding =
            ReviewProductItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ReviewBuyerViewHolder(binding)
    }

    @Suppress("DEPRECATION")
    override fun onBindViewHolder(holder: ReviewBuyerViewHolder, position: Int) {
        val data = reviewBuyer[position]
        holder.binding.tvNamaBuyer.text = data.userName
        Glide.with(holder.itemView.context).load(data.userImage).circleCrop()
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(holder.binding.ivImageBuyer)
        holder.binding.tvRatingBuyer.rating = data.userRating.toFloat()
        holder.binding.tvReviewBuyer.text = data.userReview


    }

    override fun getItemCount() = reviewBuyer.size

    class ReviewBuyerViewHolder(var binding: ReviewProductItemBinding) :
        RecyclerView.ViewHolder(binding.root)
}
