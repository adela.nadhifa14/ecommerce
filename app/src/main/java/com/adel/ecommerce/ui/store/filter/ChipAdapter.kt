package com.adel.ecommerce.ui.store.filter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.adel.ecommerce.R
import com.adel.ecommerce.databinding.ChipSliderItemBinding
import com.google.android.material.chip.Chip

class ChipAdapter(private val chip: String?) :
    RecyclerView.Adapter<ChipAdapter.ChipViewHolder>() {

    inner class ChipViewHolder(itemView: ChipSliderItemBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        fun bind(data: String) {
            itemView.findViewById<Chip>(R.id.chip_name).text = data
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChipViewHolder {
        return ChipViewHolder(
            ChipSliderItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = chip?.length!!

    override fun onBindViewHolder(holder: ChipViewHolder, position: Int) {
        holder.bind(chip!![position].toString())
    }
}