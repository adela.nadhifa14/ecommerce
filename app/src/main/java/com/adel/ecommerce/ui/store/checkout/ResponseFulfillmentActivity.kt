package com.adel.ecommerce.ui.store.checkout

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.adel.ecommerce.EcommerceRepository
import com.adel.ecommerce.databinding.ActivityResponseFulfillmentBinding
import com.adel.ecommerce.service.PaymentDataResponse
import com.adel.ecommerce.service.Rating
import com.adel.ecommerce.service.TransactionDataResponse
import com.adel.ecommerce.ui.MainActivity
import com.adel.ecommerce.ui.store.transaksi.TransactionActivity
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.hilt.android.AndroidEntryPoint
import java.text.NumberFormat
import java.util.Locale
import javax.inject.Inject

@AndroidEntryPoint
class ResponseFulfillmentActivity : AppCompatActivity() {
    private lateinit var binding: ActivityResponseFulfillmentBinding

    @Inject
    lateinit var repository: EcommerceRepository

    @Inject
    lateinit var analytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityResponseFulfillmentBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val sourceActivity = intent.getStringExtra(EXTRA_SOURCE_ACTIVITY)

        if (sourceActivity == "CheckoutActivity") {
            val dataResponse = intent.getParcelableExtra<PaymentDataResponse>(EXTRA_DATA_RESPONSE)

            binding.apply {
                dataResponse?.let {
                    tvIdTransaksi.text = dataResponse.invoiceId
                    if (dataResponse.status) {
                        tvStatus.text = "Berhasil"
                    }
                    tvTanggal.text = dataResponse.date
                    tvWaktu.text = dataResponse.time
                    tvMetodePembayaran.text = dataResponse.payment
                    val harga =
                        NumberFormat.getNumberInstance(Locale("id", "ID"))
                            .format(dataResponse.total)
                    tvTotalPembayaran.text = StringBuilder().append("Rp").append(harga)

                    val purchase = Bundle()
                    purchase.putString(
                        "FirebaseAnalytics.Param.TRANSACTION_ID",
                        dataResponse.invoiceId
                    )
                    purchase.putString(
                        "FirebaseAnalytics.Param.VALUE",
                        (dataResponse.total).toString()
                    )
                    purchase.putString("FirebaseAnalytics.Param.CURRENCY", "IDR")
                    val params = Bundle()
                    params.putParcelableArray(FirebaseAnalytics.Param.ITEMS, arrayOf(purchase))

                    analytics.logEvent(FirebaseAnalytics.Event.PURCHASE, params)

                    binding.btnSelesai.setOnClickListener {
                        val invoiceId = dataResponse.invoiceId
                        val ratingValue = rbReview.rating.toInt()
                        val reviewDesc = tvReviewDescription.text
                        repository.rating(Rating(invoiceId, ratingValue, reviewDesc.toString()))

                        repository.rating.observe(this@ResponseFulfillmentActivity) {
                            Log.d("ResponseReceiptActivity", "onCreate: $it")
                            val intentToTransaction =
                                Intent(this@ResponseFulfillmentActivity, MainActivity::class.java)
                            startActivity(intentToTransaction)
                        }
                    }
                }
            }
        } else if (sourceActivity == "TransactionActivity") {
            val dataResponse =
                intent.getParcelableExtra<TransactionDataResponse>(EXTRA_DATA_RESPONSE)

            binding.apply {
                dataResponse?.let {
                    tvIdTransaksi.text = dataResponse.invoiceId
                    if (dataResponse.status) {
                        tvStatus.text = "Berhasil"
                    }
                    tvTanggal.text = dataResponse.date
                    tvWaktu.text = dataResponse.time
                    tvMetodePembayaran.text = dataResponse.payment
                    val harga =
                        NumberFormat.getNumberInstance(Locale("id", "ID"))
                            .format(dataResponse.total)
                    tvTotalPembayaran.text = StringBuilder().append("Rp").append(harga)

                    if (dataResponse.rating != 0 || !dataResponse.review.isNullOrEmpty()) {
                        val editableReview: Editable? = dataResponse.review?.let { Editable.Factory.getInstance().newEditable(it) }
                        tvReviewDescription.text = editableReview
                        rbReview.rating = dataResponse.rating.toFloat()
                    }


                    binding.btnSelesai.setOnClickListener {
                        val invoiceId = dataResponse.invoiceId
                        val ratingValue = rbReview.rating.toInt()
                        val reviewDesc = tvReviewDescription.text
                        repository.rating(Rating(invoiceId, ratingValue, reviewDesc.toString()))

                        repository.rating.observe(this@ResponseFulfillmentActivity) {
                            Log.d("ResponseReceiptActivity", "onCreate: $it")
                            val intentToTransaction = Intent(
                                this@ResponseFulfillmentActivity,
                                TransactionActivity::class.java
                            )
                            startActivity(intentToTransaction)
                        }
                    }
                }
            }
        }

    }

    companion object {
        const val EXTRA_DATA_RESPONSE = "extra_data_response"
        const val EXTRA_SOURCE_ACTIVITY = "extra_source_activity"
    }
}