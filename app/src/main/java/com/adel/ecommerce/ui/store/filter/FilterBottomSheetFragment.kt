package com.adel.ecommerce.ui.store.filter

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import com.adel.ecommerce.databinding.FragmentFilterBottomSheetBinding
import com.adel.ecommerce.ui.store.StoreActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.chip.Chip
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FilterBottomSheetFragment : BottomSheetDialogFragment() {

    private var _binding: FragmentFilterBottomSheetBinding? = null
    private val binding get() = _binding!!

    private var chip_cat: String? = null
    private var chip_sort: String? = null
    private var highest: String? = null
    private var lowest: String? = null

    @Inject
    lateinit var analytics: FirebaseAnalytics

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFilterBottomSheetBinding.inflate(inflater, container, false)

        chip_cat = arguments?.getString(StoreActivity.EXTRA_CHIP_CAT)
        chip_sort = arguments?.getString(StoreActivity.EXTRA_CHIP_SORT)
        highest = arguments?.getString(StoreActivity.EXTRA_CHIP_HIGHEST)
        lowest = arguments?.getString(StoreActivity.EXTRA_CHIP_LOWEST)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val modalBottomSheetBehavior = (dialog as BottomSheetDialog).behavior
        dialog?.setOnShowListener {
            modalBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        binding.apply {
            buttonReset.visibility = View.GONE

            chipGroupKategori.children.forEach { chip ->
                if ((chip as Chip).text == chip_cat) {
                    chip.isChecked = true
                    buttonReset.visibility = View.VISIBLE
                }
            }

            chipGroupUrutkan.children.forEach { chip ->
                if ((chip as Chip).text == chip_sort) {
                    chip.isChecked = true
                    buttonReset.visibility = View.VISIBLE
                }
            }

            if (highest.isNullOrEmpty() || highest == "null") {
                tertinggiEditText.setText("")
            } else {
                tertinggiEditText.setText(highest)
            }

            if (lowest.isNullOrEmpty() || lowest == "null") {
                terendahEditText.setText("")
            } else {
                terendahEditText.setText(lowest)
            }

            buttonReset.setOnClickListener {
                restoreCheckedChipsState()
            }

            buttonTampilkan.setOnClickListener {
                val intentChip = Intent(activity, StoreActivity::class.java)

                val chip_sort: Chip? = chipGroupUrutkan.findViewById(chipGroupUrutkan.checkedChipId)
                val chip_cat: Chip? =
                    chipGroupKategori.findViewById(chipGroupKategori.checkedChipId)

                val highest = tertinggiEditText.text.toString()
                val lowest = terendahEditText.text.toString()

                val sort = Bundle()
                sort.putString(FirebaseAnalytics.Param.ITEM_NAME, "adel")
                sort.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, "chip_sort")
                val cat = Bundle()
                cat.putString(FirebaseAnalytics.Param.ITEM_NAME, "adel")
                cat.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, "chip_cat")
                val lowestAnalytics = Bundle()
                lowestAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, "adel")
                lowestAnalytics.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, "lowest")
                val highestAnalytics = Bundle()
                highestAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, "adel")
                highestAnalytics.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, "highest")
                val params = Bundle()
                params.putParcelableArray(
                    FirebaseAnalytics.Param.ITEMS,
                    arrayOf(sort, cat, lowestAnalytics, highestAnalytics)
                )

                analytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM, params)

                intentChip.putExtra(StoreActivity.EXTRA_CHIP_CAT, chip_cat?.text)
                intentChip.putExtra(StoreActivity.EXTRA_CHIP_SORT, chip_sort?.text)
                intentChip.putExtra(StoreActivity.EXTRA_CHIP_HIGHEST, highest)
                intentChip.putExtra(StoreActivity.EXTRA_CHIP_LOWEST, lowest)

                startActivity(intentChip)
            }
        }

    }

    private fun restoreCheckedChipsState() {
        binding.apply {

            chipGroupUrutkan.clearCheck()

            chipGroupKategori.clearCheck()

            tertinggiEditText.text?.clear()

            terendahEditText.text?.clear()

            buttonReset.visibility = View.GONE
        }
    }

    companion object {
        const val TAG = "ModalFragment"

        @JvmStatic
        fun newInstance(
            chip_cat: String,
            chip_sort: String,
            highest: String,
            lowest: String
        ): FilterBottomSheetFragment {
            val myFragment = FilterBottomSheetFragment()

            val args = Bundle().apply {
                putString(StoreActivity.EXTRA_CHIP_CAT, chip_cat)
                putString(StoreActivity.EXTRA_CHIP_SORT, chip_sort)
                putString(StoreActivity.EXTRA_CHIP_HIGHEST, highest)
                putString(StoreActivity.EXTRA_CHIP_LOWEST, lowest)
            }
            myFragment.arguments = args

            return myFragment
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}