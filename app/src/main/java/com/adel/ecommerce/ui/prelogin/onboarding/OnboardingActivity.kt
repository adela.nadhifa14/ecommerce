package com.adel.ecommerce.ui.prelogin.onboarding

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.adel.ecommerce.R
import com.adel.ecommerce.databinding.ActivityOnboardingBinding
import com.adel.ecommerce.session.AppSession
import com.adel.ecommerce.ui.prelogin.login.LoginActivity
import com.adel.ecommerce.ui.prelogin.register.RegisterActivity
import com.google.android.material.tabs.TabLayoutMediator


class OnboardingActivity : AppCompatActivity() {
    private lateinit var binding: ActivityOnboardingBinding
    private lateinit var adapter: OnboardingAdapter

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityOnboardingBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        toRegister()
        toSkip()
        getAutoSliderImage()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            splashScreen.setSplashScreenTheme(R.style.SplashScreen)
        } else {
            splashScreen.setSplashScreenTheme(R.style.SplashScreenV11)
        }
    }

    private fun toRegister() {
        binding.joinNowButton.setOnClickListener {
            AppSession.isFirstInstall = false
            val intentToRegister = Intent(this, RegisterActivity::class.java)
            startActivity(intentToRegister)
            finish()
        }
    }

    private fun toSkip() {
        binding.skipButton.setOnClickListener {
            AppSession.isFirstInstall = false
            val intentToLogin = Intent(this, LoginActivity::class.java)
            startActivity(intentToLogin)
            finish()
        }
    }


    private fun getAutoSliderImage() {
        binding.apply {

            val images =
                arrayOf(R.drawable.onboarding_1, R.drawable.onboarding_2, R.drawable.onboarding_3)

            adapter = OnboardingAdapter(images)

            onboardingVp.adapter = adapter

            TabLayoutMediator(tabDots, onboardingVp) { tab, position ->
            }.attach()

            onboardingVp.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    when (position) {
                        0 -> {
                            nextButton.setOnClickListener {
                                onboardingVp.setCurrentItem(getItemofviewpager(+1), true)
                            }
                            nextButton.visibility = View.VISIBLE
                        }

                        1 -> {
                            nextButton.setOnClickListener {
                                onboardingVp.setCurrentItem(getItemofviewpager(+1), true)
                            }
                            nextButton.visibility = View.VISIBLE
                        }

                        2 -> {
                            nextButton.visibility = View.GONE
                        }
                    }
                    super.onPageSelected(position)
                }
            })
        }
    }

    private fun getItemofviewpager(i: Int): Int {
        return binding.onboardingVp.currentItem + i
    }
}