package com.adel.ecommerce.ui.store.cart

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.adel.ecommerce.databinding.ActivityCartBinding
import com.adel.ecommerce.service.WishlistProducts
import com.adel.ecommerce.ui.store.checkout.CheckoutActivity
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.hilt.android.AndroidEntryPoint
import java.text.NumberFormat
import java.util.Locale
import javax.inject.Inject


@AndroidEntryPoint
class CartActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCartBinding
    private lateinit var adapter: CartAdapter
    private val viewModel: CartViewModel by viewModels()

    private lateinit var rootView: View

    @Inject
    lateinit var analytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityCartBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        rootView = findViewById(android.R.id.content)


        setupToolbar()
        adapter = CartAdapter(viewModel)
        binding.rvCart.layoutManager = GridLayoutManager(this, 1)
        binding.rvCart.adapter = adapter

        binding.rvCart.itemAnimator = null

        viewModel.getCartItem()?.observe(this) {
            if (it != null) {
                val productList: List<WishlistProducts> = it

                val arrayList: ArrayList<WishlistProducts> = ArrayList(productList)

                adapter.setCartItem(arrayList)
                adapter.submitList(it)

                it.forEach {
                    val viewCart = Bundle()
                    viewCart.putString(FirebaseAnalytics.Param.ITEM_ID, it.productId)
                    viewCart.putString(FirebaseAnalytics.Param.ITEM_NAME, it.productName)
                    viewCart.putString(FirebaseAnalytics.Param.VALUE, (it.quantity*it.productPrice).toString())
                    viewCart.putString(FirebaseAnalytics.Param.CURRENCY, "IDR")
                    val params = Bundle()
                    params.putParcelableArray(FirebaseAnalytics.Param.ITEMS, arrayOf(viewCart))

                    analytics.logEvent(FirebaseAnalytics.Event.VIEW_CART, params)
                }

                val totalPrice = it.filter {
                    it.selected
                }.sumOf { it.productPrice * it.quantity }


                val harga =
                    NumberFormat.getNumberInstance(Locale.getDefault()).format(totalPrice)

                binding.tvHargaCart.text = StringBuilder().append("Rp").append(harga)

                val listId = it.map { it.productId }

                val allChecked = it.all {
                    it.selected
                }

                binding.checkBox.isChecked = allChecked
                binding.checkBox.setOnCheckedChangeListener { button, isChecked ->
                    if (button.isPressed) {
                        viewModel.updateCartItemCheckbox(listId, isChecked)
                    }
                }

                adapter.setOnDeleteItemCheckedClickListener(object : CartAdapter.OnDeleteItemCheckedClickListener {
                    override fun onDeleteItemCheckedClicked(isChecked: Boolean) {
                        binding.btnHapusItem.visibility = if (isChecked) View.VISIBLE else View.GONE
                    }
                })

                binding.btnHapusItem.setOnClickListener { btn ->
                    if (allChecked) {
                        viewModel.removeFromCartAll(listId)
                    } else {
                        binding.btnHapusItem.visibility = View.VISIBLE
                        val selectedListId = it.filter { it.selected }.map { it.productId }
                        viewModel.removeFromCartAll(selectedListId)
                    }
                    binding.btnHapusItem.visibility = View.GONE
                }

                binding.btnBeliCart.setOnClickListener { click ->
                    val intentToCheckout = Intent(this, CheckoutActivity::class.java)
                    val itemSelected = it.filter { it.selected }
                    intentToCheckout.putParcelableArrayListExtra(
                        CheckoutActivity.EXTRA_PROD,
                        ArrayList(itemSelected)
                    )
                    intentToCheckout.putExtra(
                        CheckoutActivity.EXTRA_SOURCE_ACTIVITY,
                        "CartActivity"
                    )
                    startActivity(intentToCheckout)
                }
            }
            if (it.isNullOrEmpty()) {
                binding.rvCart.visibility = View.GONE
                binding.errorLayout.errorLayout.visibility = View.VISIBLE
                binding.errorLayout.errorTypeText.text = "Empty"
                binding.errorLayout.errorTypeInfo.text = "Your requested data is unavailable"
                binding.errorLayout.restartButton.visibility = View.GONE
                binding.view.visibility = View.GONE
                binding.checkBoxContainer.visibility = View.GONE
            } else {
                binding.rvCart.visibility = View.VISIBLE
                binding.errorLayout.errorLayout.visibility = View.GONE
            }

            binding.topAppBar.setNavigationOnClickListener {
                onBackPressed()
            }
        }

        adapter.setOnDeleteItemClickListener(object : CartAdapter.OnDeleteItemClickListener {
            override fun onDeleteItemClicked(productId: String) {
                viewModel.removeFromCart(productId)
                Snackbar.make(rootView, "Produk dihapus dari keranjang", Snackbar.LENGTH_SHORT)
                    .show()

                val removeFromCart = Bundle()
                removeFromCart.putString(FirebaseAnalytics.Param.ITEM_ID, productId)
                val params = Bundle()
                params.putParcelableArray(FirebaseAnalytics.Param.ITEMS, arrayOf(removeFromCart))

                analytics.logEvent(FirebaseAnalytics.Event.REMOVE_FROM_CART, params)

            }
        })
    }

    private fun setupToolbar() {
        binding.apply {
            topAppBar.title = "Keranjang"
        }
    }
}