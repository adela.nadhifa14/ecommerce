package com.adel.ecommerce.ui.store.cart

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.adel.ecommerce.service.WishlistProducts
import javax.inject.Singleton

@Database(entities = [WishlistProducts::class], version = 2)
@Singleton
abstract class CartDatabase : RoomDatabase() {
    abstract fun cartDao(): CartDAO

    companion object {
        private var INSTANCE: CartDatabase? = null

        fun getDatabase(context: Context): CartDatabase? {
            if (INSTANCE == null) {
                synchronized(this) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        CartDatabase::class.java,
                        "cart database"
                    ).fallbackToDestructiveMigration().build()
                }
            }
            return INSTANCE
        }
    }
}