package com.adel.ecommerce.ui.store.cart

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.adel.ecommerce.service.WishlistProducts

@Dao
interface CartDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addTCart(cartProducts: WishlistProducts): Long

    @Query("SELECT * FROM wishlist_products")
    fun getCartProducts(): LiveData<List<WishlistProducts>>

    @Query("UPDATE wishlist_products SET quantity = :newQuantity WHERE productId = :productId")
    suspend fun updateCartItemQuantity(productId: String, newQuantity: Int)

    @Query("UPDATE wishlist_products SET selected = :isSelected WHERE productId IN (:productId)")
    suspend fun updateCartItemCheckbox(productId: List<String>, isSelected: Boolean)

    @Query("DELETE FROM wishlist_products WHERE wishlist_products.productId = :id")
    suspend fun removeFromCart(id: String): Int

    @Query("DELETE FROM wishlist_products WHERE wishlist_products.productId IN (:productId)")
    suspend fun removeFromCartAll(productId: List<String>)
}