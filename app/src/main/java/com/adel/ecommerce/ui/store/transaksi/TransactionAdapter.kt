package com.adel.ecommerce.ui.store.transaksi


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.adel.ecommerce.databinding.TransactionItemBinding
import com.adel.ecommerce.service.TransactionDataResponse
import com.bumptech.glide.Glide
import java.text.NumberFormat
import java.util.Locale

class TransactionAdapter :
    RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>() {

    private val transactionItem = ArrayList<TransactionDataResponse>()

    fun setTransactionItem(transaction: ArrayList<TransactionDataResponse>) {
        transactionItem.clear()
        transactionItem.addAll(transaction)
    }

    private var onItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun onItemClicked(isReview: TransactionDataResponse)
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TransactionViewHolder {
        val binding =
            TransactionItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TransactionViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return transactionItem.size
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        val transactionItems = transactionItem[position]

        holder.apply {
            binding.apply {
                Glide.with(holder.itemView.context)
                    .load(transactionItems.image)
                    .into(ivBarang)
                tvTanggalTransaksi.text = transactionItems.date
                tvNamaBarang.text = transactionItems.name

                val totalQuantity = transactionItems.items?.sumOf { it.quantity } ?: 0
                tvJmlBarang.text = "$totalQuantity Barang"

                val harga = NumberFormat.getNumberInstance(Locale("id","ID"))
                    .format(transactionItems.total)
                tvTotalBelanja.text = StringBuilder().append("Rp").append(harga)

                btnReviewToResponse.visibility =
                    if (transactionItems.rating == 0 || transactionItems.review.isNullOrEmpty()) View.VISIBLE else View.GONE

                btnReviewToResponse.setOnClickListener {
                    onItemClickListener?.onItemClicked(transactionItems)
                }
            }
        }
    }

    class TransactionViewHolder(var binding: TransactionItemBinding) :
        RecyclerView.ViewHolder(binding.root)

}