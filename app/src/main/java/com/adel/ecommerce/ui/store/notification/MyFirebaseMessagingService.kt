package com.adel.ecommerce.ui.store.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.adel.ecommerce.R
import com.adel.ecommerce.service.NotificationProducts
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.util.UUID
import javax.inject.Inject

class MyFirebaseMessagingService : FirebaseMessagingService() {
    @Inject
    lateinit var viewModel: NotificationViewModel

    override fun onCreate() {
        super.onCreate()
        viewModel = NotificationViewModel(application)
    }

    override fun onNewToken(token: String) {
        Log.d("MyFirebaseMessagingService", "Refreshed token: $token")

        sendRegistrationToServer(token)
    }

    private fun sendRegistrationToServer(token: String?) {
        Log.d("MyFirebaseMessagingService", "sendRegistrationTokenToServer($token)")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        Log.d(TAG, "From: ${remoteMessage.from}")

        if (remoteMessage.data.isNotEmpty()) {
            if (remoteMessage.notification != null) {
                Log.d(TAG, "Message Notification Title: " + remoteMessage.notification?.title)
                Log.d(TAG, "Message Notification Body: " + remoteMessage.notification?.body)
            }
            Log.d(TAG, "Message data payload: ${remoteMessage.data}")
        }

        sendNotification(remoteMessage)

    }

    private fun sendNotification(remoteMessage: RemoteMessage) {
        val title = remoteMessage.notification?.title
        val description = remoteMessage.notification?.body

        val intent = Intent(this, NotificationActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val requestCode = 0
        val pendingIntent = PendingIntent.getActivity(
            this,
            requestCode,
            intent,
            PendingIntent.FLAG_IMMUTABLE,
        )

        val channelId = "TokoPhincon"
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(title)
            .setContentText(description)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)
            .setStyle(NotificationCompat.BigTextStyle().bigText(description))

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT,
            )
            notificationManager.createNotificationChannel(channel)
        }

        val notificationId = System.currentTimeMillis().toInt()
        notificationManager.notify(notificationId, notificationBuilder.build())

        val notifItem = NotificationProducts(
            title = title!!,
            body = description!!,
            date = "${remoteMessage.data["date"]}, ${remoteMessage.data["time"]}",
            id = UUID.randomUUID().toString(),
            image = remoteMessage.notification!!.imageUrl.toString(),
            type = remoteMessage.data["type"]!!,
            isRead = false
        )

        viewModel.addToNotification(
            notifItem
        )

        Log.d(TAG, "sendNotification: $notifItem")
    }

    companion object {
        private const val TAG = "MyFirebaseMsgService"
    }


}