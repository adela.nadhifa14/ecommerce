package com.adel.ecommerce.ui.store.wishlist

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.adel.ecommerce.service.WishlistProducts
import javax.inject.Singleton

@Database(entities = [WishlistProducts::class], version = 2)
@Singleton
abstract class WishlistDatabase : RoomDatabase() {
    abstract fun wishlistItemDao(): WishlistDAO

    companion object {
        private var INSTANCE: WishlistDatabase? = null

        fun getDatabase(context: Context): WishlistDatabase? {
            if (INSTANCE == null) {
                synchronized(this) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        WishlistDatabase::class.java,
                        "wishlist database"
                    ).fallbackToDestructiveMigration().build()
                }
            }
            return INSTANCE
        }
    }
}