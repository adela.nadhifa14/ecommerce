package com.adel.ecommerce.ui.store.search

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.adel.ecommerce.databinding.SearchProductsItemBinding

class SearchAdapter : RecyclerView.Adapter<SearchAdapter.SearchViewHolder>() {

    private val search = ArrayList<String>()

    private lateinit var onItemClickCallback: OnItemClickCallback

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setSearchProducts(product: ArrayList<String>) {
        search.clear()
        search.addAll(product)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val binding =
            SearchProductsItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SearchViewHolder(binding)
    }

    @Suppress("DEPRECATION")
    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.binding.productName.text = search[position]

        Log.d("SearchAdapter", "What's in the server? : ${holder.binding.productName.text}")

        holder.itemView.setOnClickListener {
            onItemClickCallback.onItemClicked(search[holder.adapterPosition])
        }
    }

    override fun getItemCount() = search.size

    class SearchViewHolder(var binding: SearchProductsItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    interface OnItemClickCallback {
        fun onItemClicked(data: String)
    }
}
