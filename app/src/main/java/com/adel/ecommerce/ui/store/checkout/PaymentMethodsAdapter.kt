package com.adel.ecommerce.ui.store.checkout

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.adel.ecommerce.databinding.PaymentItemBinding
import com.adel.ecommerce.service.PaymentMethodItemResponse
import com.bumptech.glide.Glide

class PaymentMethodsAdapter :
    RecyclerView.Adapter<PaymentMethodsAdapter.PaymentViewHolder>() {

    private val paymentItem = ArrayList<PaymentMethodItemResponse>()

    fun setPaymentMethods(payment: ArrayList<PaymentMethodItemResponse>) {
        paymentItem.clear()
        paymentItem.addAll(payment)
    }

    private var onItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun onItemClicked(label: PaymentMethodItemResponse)
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PaymentViewHolder {
        val binding =
            PaymentItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PaymentViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return paymentItem.size
    }

    override fun onBindViewHolder(holder: PaymentViewHolder, position: Int) {
        val (label, image, status) = paymentItem[position]

        holder.apply {
            binding.apply {
                Glide.with(holder.itemView.context)
                    .load(image)
                    .into(bankImage)
                bankName.text = label

                itemView.setOnClickListener {
                    paymentItem[position].let { onItemClickListener?.onItemClicked(it) }
                }

                if (!status) {
                    itemView.isClickable = false
                    itemView.alpha = 0.3f
                }
            }
        }
    }

    class PaymentViewHolder(var binding: PaymentItemBinding) :
        RecyclerView.ViewHolder(binding.root)

}