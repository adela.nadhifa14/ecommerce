package com.adel.ecommerce.ui.store

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.adel.ecommerce.R
import com.adel.ecommerce.databinding.ActivityStoreBinding
import com.adel.ecommerce.service.GetProductsItemResponse
import com.adel.ecommerce.session.SessionPreference
import com.adel.ecommerce.ui.MainActivity
import com.adel.ecommerce.ui.prelogin.login.LoginActivity
import com.adel.ecommerce.ui.store.cart.CartActivity
import com.adel.ecommerce.ui.store.cart.CartViewModel
import com.adel.ecommerce.ui.store.detailProduct.DetailProductActivity
import com.adel.ecommerce.ui.store.filter.BottomLoadingStateAdapter
import com.adel.ecommerce.ui.store.filter.FilterBottomSheetFragment
import com.adel.ecommerce.ui.store.filter.FilterBottomSheetFragment.Companion.TAG
import com.adel.ecommerce.ui.store.notification.NotificationActivity
import com.adel.ecommerce.ui.store.pagingStore.PagingProductsViewModel
import com.adel.ecommerce.ui.store.pagingStore.ProductPagingAdapter
import com.adel.ecommerce.ui.store.search.SearchAdapter
import com.adel.ecommerce.ui.store.search.SearchViewModel
import com.adel.ecommerce.ui.store.transaksi.TransactionActivity
import com.adel.ecommerce.ui.store.wishlist.WishlistActivity
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.google.android.material.badge.ExperimentalBadgeUtils
import com.google.android.material.chip.Chip
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.hilt.android.AndroidEntryPoint
import java.lang.StringBuilder
import java.text.NumberFormat
import java.util.Locale
import javax.inject.Inject


@AndroidEntryPoint
class StoreActivity : AppCompatActivity() {
    private lateinit var binding: ActivityStoreBinding
    private lateinit var pagingAdapter: ProductPagingAdapter
    private lateinit var loadStateAdapter: BottomLoadingStateAdapter
    private val searchAdapter: SearchAdapter by lazy { SearchAdapter() }
    private val viewModel: SearchViewModel by viewModels()
    private val cartViewModel: CartViewModel by viewModels()
    private val pagingViewModel by viewModels<PagingProductsViewModel>()

    private val searchDelayMillis = 1000L
    private var searchHandler: Handler = Handler()
    private var searchRunnable: Runnable? = null

    private var chip_cat: String? = null
    private var chip_sort: String? = null
    private var highest: String? = null
    private var lowest: String? = null

    @Inject
    lateinit var analytics: FirebaseAnalytics

    @ExperimentalBadgeUtils
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityStoreBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setupToolbar()
        setupBottomNav()
        toFilterBottomSheet()
        toSearchPage()
        convertLayout()
        enterChip()

        cartViewModel.getCartItem()?.observe(this) {
            binding.topAppBar.viewTreeObserver.addOnGlobalLayoutListener {
                val badgeDrawable = BadgeDrawable.create(this).apply {
                    isVisible = it.isNotEmpty()
                    number = it.size
                }
                BadgeUtils.attachBadgeDrawable(
                    badgeDrawable,
                    binding.topAppBar,
                    R.id.toolbar_cart
                )
            }
        }
    }

    private fun convertLayout() {
        binding.apply {

            pagingAdapter = ProductPagingAdapter()

            recyclerView.adapter = pagingAdapter

            val searchQuery = intent.getStringExtra(EXTRA_SEARCH)
            val brandQuery = intent.getStringExtra(EXTRA_CHIP_CAT)
            val sortQuery = intent.getStringExtra(EXTRA_CHIP_SORT)
            val lowestQuery = intent.getStringExtra(EXTRA_CHIP_LOWEST)?.toIntOrNull()
            val higestQuery = intent.getStringExtra(EXTRA_CHIP_HIGHEST)?.toIntOrNull()

            var layoutManager: GridLayoutManager? = null

            pagingViewModel.allProducts(
                searchQuery, brandQuery, lowestQuery, higestQuery, sortQuery
            ).observe(this@StoreActivity) { product ->
                if (product != null) {
                    // Create the layout manager if it hasn't been initialized
                    if (layoutManager == null) {
                        layoutManager = GridLayoutManager(this@StoreActivity, 1)
                        recyclerView.layoutManager = layoutManager
                    }

                    pagingAdapter.submitData(lifecycle, product)

                    val itemStore = Bundle()
                    itemStore.putString(FirebaseAnalytics.Param.ITEM_NAME, "adel")
                    itemStore.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, "item store")
                    val params = Bundle()
                    params.putParcelableArray(FirebaseAnalytics.Param.ITEMS, arrayOf(itemStore))

                    analytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM_LIST, params)

                    convertButton.setOnClickListener {
                        pagingAdapter.item = !pagingAdapter.item
                        if (pagingAdapter.item) {
                            convertButton.setImageResource(R.drawable.baseline_list_alt_24)
                            layoutManager?.spanCount = 1
                        } else {
                            convertButton.setImageResource(R.drawable.baseline_grid_view_24)
                            layoutManager?.spanCount = 2
                        }
                        pagingAdapter.notifyItemRangeChanged(0, pagingAdapter.itemCount)
                    }
                }
            }


            pagingAdapter.setOnItemClickCallback(object : ProductPagingAdapter.OnItemClickCallback {
                override fun onItemClicked(data: GetProductsItemResponse) {
                    val intentToDetail =
                        Intent(this@StoreActivity, DetailProductActivity::class.java)
                    intentToDetail.putExtra(DetailProductActivity.EXTRA_ID_DETAIL, data.productId)
                    intentToDetail.putExtra(DetailProductActivity.EXTRA_NAME_PROD, data.productName)
                    intentToDetail.putExtra(
                        DetailProductActivity.EXTRA_PRICE_PROD,
                        data.productPrice
                    )
                    intentToDetail.putExtra(DetailProductActivity.EXTRA_IMAGE_PROD, data.image)
                    intentToDetail.putExtra(DetailProductActivity.EXTRA_STORE_PROD, data.store)
                    intentToDetail.putExtra(DetailProductActivity.EXTRA_SALE_PROD, data.sale)
                    intentToDetail.putExtra(
                        DetailProductActivity.EXTRA_RATING_PROD,
                        data.productRating
                    )
                    startActivity(intentToDetail)

                    val selectItem = Bundle()
                    selectItem.putString(FirebaseAnalytics.Param.ITEM_LIST_ID, data.productId)
                    selectItem.putString(FirebaseAnalytics.Param.ITEM_NAME, data.productName)
                    val params = Bundle()
                    params.putParcelableArray(FirebaseAnalytics.Param.ITEMS, arrayOf(selectItem))

                    analytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM, params)
                }
            })


            loadStateAdapter = BottomLoadingStateAdapter()

            pagingAdapter.withLoadStateHeaderAndFooter(
                header = BottomLoadingStateAdapter(), footer = BottomLoadingStateAdapter()
            )

            pagingAdapter.addLoadStateListener { loadStates: CombinedLoadStates ->
                val isRefreshing = loadStates.refresh is LoadState.Loading
                val isLoadingMore = loadStates.append is LoadState.Loading

                if (isRefreshing || isLoadingMore) {
                    showBottomLoadingBar()
                } else {
                    hideBottomLoadingBar()
                }

                shimmerLinear.visibility =
                    if (loadStates.refresh is LoadState.Loading && pagingAdapter.item) View.VISIBLE else View.GONE
                shimmerGrid.visibility =
                    if (loadStates.refresh is LoadState.Loading && !pagingAdapter.item) View.VISIBLE else View.GONE
                shimmerFilter.visibility =
                    if (loadStates.refresh is LoadState.Loading && pagingAdapter.item) View.VISIBLE else if (loadStates.refresh is LoadState.Loading && !pagingAdapter.item) View.VISIBLE else View.GONE

                recyclerView.visibility =
                    if (loadStates.refresh is LoadState.Loading) View.GONE else View.VISIBLE
                filterBottomSheet.visibility =
                    if (loadStates.refresh is LoadState.Loading) View.GONE else View.VISIBLE
                convertButton.visibility =
                    if (loadStates.refresh is LoadState.Loading) View.GONE else View.VISIBLE
                divider.visibility =
                    if (loadStates.refresh is LoadState.Loading) View.GONE else View.VISIBLE
                chipGroupScrollable.visibility =
                    if (loadStates.refresh is LoadState.Loading) View.GONE else View.VISIBLE

                if (loadStates.refresh is LoadState.Error) {
                    val error = (loadStates.refresh as LoadState.Error).error
                    val errorMessage = error.message

                    recyclerView.visibility = View.GONE

                    if (errorMessage?.contains("404") == true) {
                        errorLayout.errorTypeText.text = "Empty"
                        errorLayout.errorTypeInfo.text = "Your requested data is unavailable"
                        errorLayout.restartButton.text = "Reset"
                        errorLayout.restartButton.setOnClickListener {
                            pagingViewModel.allProducts().observe(this@StoreActivity) { product ->
                                if (product != null) {
                                    pagingAdapter.submitData(lifecycle, product)
                                    recyclerView.layoutManager =
                                        GridLayoutManager(this@StoreActivity, 1)
                                }
                            }
                        }
                    } else {
                        errorLayout.errorTypeText.text = "500"
                        errorLayout.errorTypeInfo.text = "Internal Server Error"
                        errorLayout.restartButton.text = "Refresh"
                        errorLayout.restartButton.setOnClickListener {
                            pagingAdapter.refresh()
                        }
                    }
                }

//                val error: Throwable = (loadStates.refresh as LoadState.Error).error
//                when (error) {
//                    is HttpException -> {
//                        val code = error.code()
//                        val message = error.message
//                        if (code == 404) {
//                            errorLayout.errorTypeText.text = "Empty"
//                            errorLayout.errorTypeInfo.text = "Your requested data is unavailable"
//                            errorLayout.restartButton.text = "Reset"
//                            errorLayout.restartButton.setOnClickListener {
//                                pagingViewModel.allProducts()
//                                    .observe(this@StoreActivity) { product ->
//                                        if (product != null) {
//                                            pagingAdapter.submitData(lifecycle, product)
//                                            recyclerView.layoutManager =
//                                                GridLayoutManager(this@StoreActivity, 1)
//                                        }
//                                    }
//                            }
//                        } else {
//                            val errorResponse = error.response()?.errorBody()?.string()
//                        }
//                    }
//                }

                errorLayout.errorLayout.visibility =
                    if (loadStates.refresh is LoadState.Error) View.VISIBLE else View.GONE

            }
            recyclerView.adapter = pagingAdapter

            swipeRefreshLayout.setOnRefreshListener {
                pagingAdapter.refresh()
                swipeRefreshLayout.isRefreshing = false
            }
        }
    }

    private fun showBottomLoadingBar() {
        val inflater = LayoutInflater.from(this)
        val bottomLoadingView = inflater.inflate(R.layout.bottom_loading, binding.bottomLoadingContainer, false)
        binding.bottomLoadingContainer.removeAllViews()
        binding.bottomLoadingContainer.addView(bottomLoadingView)
    }

    private fun hideBottomLoadingBar() {
        binding.bottomLoadingContainer.removeAllViews()
    }

    private fun setupToolbar() {
        binding.apply {
            val sessionPreference = SessionPreference(this@StoreActivity)
            topAppBar.title = sessionPreference.passUserName().toString()

            topAppBar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.toolbar_notifications -> {
                        val intentToNotifs =
                            Intent(this@StoreActivity, NotificationActivity::class.java)
                        startActivity(intentToNotifs)
                        true
                    }

                    R.id.toolbar_cart -> {
                        val intentToCart = Intent(this@StoreActivity, CartActivity::class.java)
                        startActivity(intentToCart)
                        true
                    }

                    R.id.toolbar_menu -> {
                        true
                    }

                    else -> false
                }
            }
        }
    }

    private fun setupBottomNav() {
        binding.apply {
            navView.selectedItemId = R.id.navigation_store

            navView.setOnItemSelectedListener { item ->
                when (item.itemId) {
                    R.id.navigation_store -> {
                        return@setOnItemSelectedListener true
                    }

                    R.id.navigation_home -> {
                        startActivity(Intent(this@StoreActivity, MainActivity::class.java))
                        overridePendingTransition(0, 0)
                        return@setOnItemSelectedListener true
                    }

                    R.id.navigation_wishlist -> {
                        startActivity(Intent(applicationContext, WishlistActivity::class.java))
                        overridePendingTransition(0, 0)
                        return@setOnItemSelectedListener true
                    }

                    R.id.navigation_transaksi -> {
                        startActivity(Intent(applicationContext, TransactionActivity::class.java))
                        overridePendingTransition(0, 0)
                        return@setOnItemSelectedListener true
                    }

                }
                return@setOnItemSelectedListener false
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged", "ClickableViewAccessibility")
    private fun toSearchPage() {
        binding.apply {
            val componentsToHide = arrayOf(
                topAppBar,
                navView,
                divider,
                recyclerView,
                filterBottomSheet,
                chipGroupScrollable,
                convertButton,
                shimmerLinear,
                shimmerGrid
            )

            var isFirstClick = true

            searchEditText.setOnTouchListener { _, _ ->
                if (isFirstClick) {
                    componentsToHide.forEach { it.visibility = View.GONE }
                    searchRecyclerView.visibility = View.VISIBLE
                    isFirstClick = false
                }
                false
            }

            searchEditText.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    componentsToHide.forEach { it.visibility = View.GONE }
                    searchRecyclerView.visibility = View.VISIBLE
                }
            }



            searchEditText.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?, start: Int, count: Int, after: Int
                ) {
                }

                override fun onTextChanged(
                    s: CharSequence?, start: Int, before: Int, count: Int
                ) {
                    searchRunnable?.let { it1 -> searchHandler.removeCallbacks(it1) }
                    progressBar.isVisible = true
                }

                override fun afterTextChanged(s: Editable?) {
                    progressBar.isVisible = false

                    searchRunnable = Runnable {
                        viewModel.search(s.toString())

                        val bundle = Bundle()

                        bundle.putString(FirebaseAnalytics.Param.SEARCH_TERM, s.toString())

                        analytics.logEvent(FirebaseAnalytics.Event.VIEW_SEARCH_RESULTS, bundle)

                    }
                    searchHandler.postDelayed(searchRunnable!!, searchDelayMillis)

                    searchRecyclerView.adapter = searchAdapter
                    viewModel.searchUser.observe(this@StoreActivity) { search ->
                        progressBar.isVisible = false
                        if (search != null) {
                            searchAdapter.setSearchProducts(ArrayList(search))
                            searchRecyclerView.layoutManager =
                                GridLayoutManager(this@StoreActivity, 1)
                        }
                    }

                    searchEditText.setOnEditorActionListener { _, actionId, _ ->
                        return@setOnEditorActionListener when (actionId) {
                            EditorInfo.IME_ACTION_SEND -> {
                                pagingViewModel.allProducts(s.toString())
                                    .observe(this@StoreActivity) { product ->
                                        if (product != null) {
                                            searchRecyclerView.visibility = View.GONE
                                            topAppBar.visibility = View.VISIBLE
                                            navView.visibility = View.VISIBLE
                                            divider.visibility = View.VISIBLE
                                            pagingAdapter.submitData(lifecycle, product)
                                            recyclerView.layoutManager =
                                                GridLayoutManager(this@StoreActivity, 1)
                                        }
                                    }
                                true
                            }

                            else -> false
                        }
                    }

                    searchAdapter.setOnItemClickCallback(object :
                        SearchAdapter.OnItemClickCallback {
                        override fun onItemClicked(data: String) {
                            val intentToStore =
                                Intent(this@StoreActivity, StoreActivity::class.java)
                            intentToStore.putExtra(EXTRA_SEARCH, data)
                            startActivity(intentToStore)
                        }
                    })
                }
            })

        }
    }

    private fun toFilterBottomSheet() {
        binding.apply {
            filterBottomSheet.setOnClickListener {
                val modalBottomSheet = FilterBottomSheetFragment.newInstance(
                    chip_cat.toString(),
                    chip_sort.toString(),
                    highest.toString(),
                    lowest.toString()
                )
                modalBottomSheet.show(supportFragmentManager, TAG)

                Log.d(
                    "filter",
                    "Selected chips are filter : cat : $chip_cat & sort : $chip_sort & highest : $highest & lowest : $lowest"
                )
            }
        }
    }

    private fun enterChip() {
        chip_cat = intent.getStringExtra(EXTRA_CHIP_CAT)
        chip_sort = intent.getStringExtra(EXTRA_CHIP_SORT)
        highest = intent.getStringExtra(EXTRA_CHIP_HIGHEST)
        lowest = intent.getStringExtra(EXTRA_CHIP_LOWEST)

        Log.d(
            "StoreActivity",
            "Selected chips are filter : cat : $chip_cat & sort : $chip_sort & highest : $highest & lowest : $lowest"
        )

        if (chip_cat != null) {
            intentChip(chip_cat!!)
        }
        if (chip_sort != null) {
            intentChip(chip_sort!!)
        }
        if (!lowest.isNullOrEmpty()) {
            val lowestValue = lowest!!.toDoubleOrNull()
            if (lowestValue != null) {
                val lowestFormat = NumberFormat.getNumberInstance(Locale("id","ID")).format(lowestValue)
                val lowestRp = StringBuilder().append("> ").append("Rp").append(lowestFormat).toString()
                intentChip(lowestRp)
            }
        }
        if (!highest.isNullOrEmpty()) {
            val highestValue = highest!!.toDoubleOrNull()
            if (highestValue != null) {
                val highestFormat = NumberFormat.getNumberInstance(Locale("id","ID")).format(highestValue)
                val highestRp = StringBuilder().append("< ").append("Rp").append(highestFormat).toString()
                intentChip(highestRp)
            }
        }

    }

    private fun intentChip(name: String) {
        val chip = Chip(this)
        chip.apply {
            text = name
            isChipIconVisible = false
            isCloseIconVisible = false
            isCheckable = true
            binding.apply {
                chipGroupScrollable.addView(chip as View)
            }
        }
    }

    companion object {
        const val EXTRA_SEARCH = "extra_search_ads"
        const val EXTRA_CHIP_CAT = "extra_chip_cat"
        const val EXTRA_CHIP_SORT = "extra_chip_sort"
        const val EXTRA_CHIP_LOWEST = "extra_chip_lowest"
        const val EXTRA_CHIP_HIGHEST = "extra_chip_highest"
    }
}