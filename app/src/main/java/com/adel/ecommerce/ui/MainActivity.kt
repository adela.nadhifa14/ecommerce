package com.adel.ecommerce.ui

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.adel.ecommerce.R
import com.adel.ecommerce.databinding.ActivityMainBinding
import com.adel.ecommerce.service.AuthAuthenticator
import com.adel.ecommerce.session.SessionPreference
import com.adel.ecommerce.ui.prelogin.login.LoginActivity
import com.adel.ecommerce.ui.prelogin.profile.ProfileActivity
import com.adel.ecommerce.ui.store.StoreActivity
import com.adel.ecommerce.ui.store.cart.CartActivity
import com.adel.ecommerce.ui.store.cart.CartViewModel
import com.adel.ecommerce.ui.store.notification.NotificationActivity
import com.adel.ecommerce.ui.store.transaksi.TransactionActivity
import com.adel.ecommerce.ui.store.wishlist.WishlistActivity
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.google.android.material.badge.ExperimentalBadgeUtils
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val viewModel: CartViewModel by viewModels()

    private val MY_PERMISSION_REQUEST_CODE = 1

    @Inject
    lateinit var remoteConfig: FirebaseRemoteConfig

    @Inject
    lateinit var messaging: FirebaseMessaging

    @Inject
    lateinit var authAuthenticator: AuthAuthenticator

    @ExperimentalBadgeUtils
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        installSplashScreen()
        setContentView(binding.root)

        checkIfUserLoggedIn()
        checkIfUserHaveSetProfile()
        getNotificationPermission()
        setupToolbar()
        setupBottomNav()

        remoteConfig.fetchAndActivate()
        messaging.subscribeToTopic("promo")

        viewModel.getCartItem()?.observe(this) {
            binding.topAppBar.viewTreeObserver.addOnGlobalLayoutListener {
                val badgeDrawable = BadgeDrawable.create(this).apply {
                    isVisible = it.isNotEmpty()
                    number = it.size
                }
                BadgeUtils.attachBadgeDrawable(
                    badgeDrawable,
                    binding.topAppBar,
                    R.id.toolbar_cart
                )
            }
        }

        binding.logoutBtn.setOnClickListener {
            val loginSession = SessionPreference(this@MainActivity)

            loginSession.logOut()

            startActivity(Intent(this@MainActivity, LoginActivity::class.java))
            finish()
        }

        authAuthenticator.logoutEvent.observe(this) {
            val loginSession = SessionPreference(this@MainActivity)

            loginSession.logOut()

            startActivity(Intent(this@MainActivity, LoginActivity::class.java))
            finish()
        }

    }

    private fun checkIfUserHaveSetProfile() {
        val userSession = SessionPreference(this)

        if (userSession.passUserName() == null && userSession.passUserImage() == null
        ) {
            if (userSession.passAccessToken() != null) {
                startActivity(Intent(this, ProfileActivity::class.java))
                finish()
            }
        }
    }

    private fun checkIfUserLoggedIn() {
        val userSession = SessionPreference(this)

        if (userSession.passAccessToken() == null) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    private fun getNotificationPermission() {
        if (checkSelfPermission(android.Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                requestPermissions(
                    arrayOf(android.Manifest.permission.POST_NOTIFICATIONS),
                    MY_PERMISSION_REQUEST_CODE
                )
            }
        }
    }

    private fun setupBottomNav() {
        val bottomNav = binding.navView

        bottomNav.selectedItemId = R.id.navigation_home

        bottomNav.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    return@setOnItemSelectedListener true
                }

                R.id.navigation_store -> {
                    startActivity(Intent(applicationContext, StoreActivity::class.java))
                    overridePendingTransition(0, 0)
                    return@setOnItemSelectedListener true
                }

                R.id.navigation_wishlist -> {
                    startActivity(Intent(applicationContext, WishlistActivity::class.java))
                    overridePendingTransition(0, 0)
                    return@setOnItemSelectedListener true
                }

                R.id.navigation_transaksi -> {
                    startActivity(Intent(applicationContext, TransactionActivity::class.java))
                    overridePendingTransition(0, 0)
                    return@setOnItemSelectedListener true
                }
            }
            return@setOnItemSelectedListener false
        }
    }


    private fun setupToolbar() {
        binding.apply {
            topAppBar.setNavigationOnClickListener {
                // Handle navigation icon press
            }

            val sessionPreference = SessionPreference(this@MainActivity)
            topAppBar.title = sessionPreference.passUserName().toString()

            topAppBar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.toolbar_notifications -> {
                        val intentToNotifs =
                            Intent(this@MainActivity, NotificationActivity::class.java)
                        startActivity(intentToNotifs)
                        true
                    }

                    R.id.toolbar_cart -> {
                        val intentToCart = Intent(this@MainActivity, CartActivity::class.java)
                        startActivity(intentToCart)
                        true
                    }

                    R.id.toolbar_menu -> {
                        true
                    }

                    else -> false
                }
            }
        }
    }
}