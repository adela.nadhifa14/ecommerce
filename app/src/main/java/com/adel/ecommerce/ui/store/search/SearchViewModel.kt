package com.adel.ecommerce.ui.store.search

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adel.ecommerce.service.ApiService
import com.adel.ecommerce.service.SearchResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val apiService: ApiService
) : ViewModel() {

    val _searchUser = MutableLiveData<List<String>>()
    val searchUser: LiveData<List<String>> = _searchUser

    fun search(query: String) {
        apiService.doSearch(query)
            .enqueue(object : Callback<SearchResponse> {
                override fun onResponse(
                    call: Call<SearchResponse>,
                    response: Response<SearchResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        _searchUser.value = response.body()!!.data
                        Log.d(TAG, response.toString())
                    }
                }

                override fun onFailure(call: Call<SearchResponse>, t: Throwable) {
                    Log.d(TAG, "Response : ${t.message.toString()}")
                }
            })
    }

    companion object {
        const val TAG = "SearchViewModel"
    }
}