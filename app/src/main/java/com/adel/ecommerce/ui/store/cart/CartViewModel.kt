package com.adel.ecommerce.ui.store.cart

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.adel.ecommerce.service.WishlistProducts
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(
    application: Application
) : ViewModel() {

    private var cartDb: CartDatabase? = CartDatabase.getDatabase(application)
    private var cartDAO = cartDb?.cartDao()

    fun addToCart(
        productId: String,
        productName: String,
        productPrice: Int,
        image: String,
        stock: Int?,
        variantName: String,
        variantPrice: Int?,
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            val wlItem = WishlistProducts(
                productId,
                productName,
                productPrice,
                image,
                "",
                "",
                "",
                0,
                stock,
                0,
                0,
                0,
                0F,
                variantName,
                variantPrice
            )
            cartDAO?.addTCart(wlItem)
        }
    }

    fun getCartItem(): LiveData<List<WishlistProducts>>? {
        return cartDAO?.getCartProducts()
    }

    fun removeFromCart(id: String) {
        CoroutineScope(Dispatchers.IO).launch {
            cartDAO?.removeFromCart(id)
        }
    }

    fun removeFromCartAll(productId: List<String>) {
        CoroutineScope(Dispatchers.IO).launch {
            cartDAO?.removeFromCartAll(productId)
        }
    }

    fun updateCartItemQuantity(productId: String, newQuantity: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            cartDAO?.updateCartItemQuantity(productId, newQuantity)
        }
    }

    fun updateCartItemCheckbox(productId: List<String>, isSelected: Boolean) {
        CoroutineScope(Dispatchers.IO).launch {
            cartDAO?.updateCartItemCheckbox(productId, isSelected)
        }
    }

}