package com.adel.ecommerce.ui.prelogin.profile

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.widget.addTextChangedListener
import com.adel.ecommerce.R
import com.adel.ecommerce.databinding.ActivityProfileBinding
import com.adel.ecommerce.ui.MainActivity
import com.adel.ecommerce.utils.reduceFileImage
import com.adel.ecommerce.utils.uriToFile
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File

@AndroidEntryPoint
class ProfileActivity : AppCompatActivity() {
    private lateinit var binding: ActivityProfileBinding
    private val viewModel: ProfileViewModel by viewModels()
    private lateinit var currentPhotoPath: String
    private var getFile: File? = null

    companion object {

        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
        private const val REQUEST_CODE_PERMISSIONS = 10
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityProfileBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        saveToMain()
        setupAction()
        syaratKetentuan()

        if (!allPermissionsGranted()) {
            ActivityCompat.requestPermissions(
                this,
                REQUIRED_PERMISSIONS,
                REQUEST_CODE_PERMISSIONS
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (!allPermissionsGranted()) {
                Toast.makeText(
                    this,
                    "Tidak mendapatkan permission.",
                    Toast.LENGTH_SHORT
                ).show()
                finish()
            }
        }
    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(baseContext, it) == PackageManager.PERMISSION_GRANTED
    }

    private fun setMyButtonEnable() {
        binding.apply {
            val nameSet = userName.text
            finishBtn.isEnabled =
                nameSet != null && nameSet.toString()
                    .isNotEmpty()
        }
    }

    private fun setupAction() {
        binding.apply {
            userName.addTextChangedListener {
                setMyButtonEnable()
            }

            finishBtn.setOnClickListener {
                val file = reduceFileImage(getFile)

                val requestUser =
                    MultipartBody.Part.createFormData("userName", userName.text.toString())
                val requestImageFile = file.asRequestBody("image/*".toMediaTypeOrNull())

                val userImage: MultipartBody.Part? = if (file != null) {
                    val userImageRequestBody = MultipartBody.Part.createFormData(
                        "userImage",
                        file.name,
                        requestImageFile
                    )
                    userImageRequestBody
                } else {
                    null
                }

                userImage?.let { it1 -> viewModel.doUploadProfile(it1, requestUser) }
                AlertDialog.Builder(this@ProfileActivity).apply {
                    setTitle("Yeah!")
                    setMessage("Successfully uploading name and saveToProfile image. Let's get you in!")
                    setPositiveButton("Continue") { _, _ ->
                        val intent = Intent(context, MainActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        finish()
                    }
                    create()
                    show()
                }
            }
        }
    }

    private fun saveToMain() {
        binding.finishBtn.setOnClickListener {
            val toMain = Intent(this, MainActivity::class.java)
            startActivity(toMain)
            finish()
        }
    }

    fun openProfilPicture(view: View) {
        MaterialAlertDialogBuilder(this@ProfileActivity)
            .setTitle(resources.getString(R.string.choose_image))
            .setNegativeButton(resources.getString(R.string.camera)) { dialog, which ->
                startTakePhoto()
            }
            .setPositiveButton(resources.getString(R.string.gallery)) { dialog, which ->
                startGallery()
            }
            .show()
    }

    private fun startGallery() {
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        intent.type = "image/*"
        val chooser = Intent.createChooser(intent, "Choose a Picture")
        launcherIntentGallery.launch(chooser)
    }

    private fun startTakePhoto() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.resolveActivity(packageManager)

        com.adel.ecommerce.utils.createTempFile(application).also {
            val photoURI: Uri = FileProvider.getUriForFile(
                this,
                "com.adel.ecommerce",
                it
            )
            currentPhotoPath = it.absolutePath
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
            launcherIntentCamera.launch(intent)
        }
    }

    private val launcherIntentCamera = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) {
        if (it.resultCode == RESULT_OK) {
            val myFile = File(currentPhotoPath)
            getFile = myFile

            val result = BitmapFactory.decodeFile(getFile?.path)

            Glide.with(applicationContext)
                .load(result)
                .circleCrop()
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(binding.userImage)
        }
    }


    private val launcherIntentGallery = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        if (result.resultCode == RESULT_OK) {
            val selectedImg: Uri = result.data?.data as Uri

            val myFile = uriToFile(selectedImg, this)

            getFile = myFile

            Glide.with(applicationContext)
                .load(selectedImg)
                .circleCrop()
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(binding.userImage)
        }
    }

    private fun syaratKetentuan() {
        binding.apply {
            val text = resources.getString(R.string.terms_and_conditions)
            val spannableString = SpannableStringBuilder(text)

            val colorSpan1 = ForegroundColorSpan(Color.parseColor("#A0A0A0"))
            spannableString.setSpan(colorSpan1, 0, 37, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

            val colorSpan2 = ForegroundColorSpan(Color.parseColor("#6750A4"))
            spannableString.setSpan(colorSpan2, 37, 55, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

            val colorSpan3 = ForegroundColorSpan(Color.parseColor("#A0A0A0"))
            spannableString.setSpan(colorSpan3, 56, 62, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

            val colorSpan4 = ForegroundColorSpan(Color.parseColor("#6750A4"))
            spannableString.setSpan(colorSpan4, 63, 80, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

            val colorSpan5 = ForegroundColorSpan(Color.parseColor("#A0A0A0"))
            spannableString.setSpan(colorSpan5, 81, 92, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

            syaratKetentuan.text = spannableString
        }
    }
}

//package com.adel.ecommerce.ui.prelogin.profile
//
//import android.Manifest
//import android.content.ActivityNotFoundException
//import android.content.Intent
//import android.content.pm.PackageManager
//import android.graphics.Bitmap
//import android.graphics.Color
//import android.os.Bundle
//import android.os.Environment
//import android.provider.MediaStore
//import android.text.Spannable
//import android.text.SpannableStringBuilder
//import android.text.style.ForegroundColorSpan
//import android.view.View
//import android.widget.Toast
//import androidx.activity.viewModels
//import androidx.appcompat.app.AlertDialog
//import androidx.appcompat.app.AppCompatActivity
//import androidx.core.content.ContextCompat
//import androidx.core.widget.addTextChangedListener
//import com.adel.ecommerce.R
//import com.adel.ecommerce.databinding.ActivityProfileBinding
//import com.adel.ecommerce.ui.MainActivity
//import com.bumptech.glide.Glide
//import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
//import com.google.android.material.dialog.MaterialAlertDialogBuilder
//import dagger.hilt.android.AndroidEntryPoint
//import okhttp3.MediaType.Companion.toMediaTypeOrNull
//import okhttp3.MultipartBody
//import okhttp3.RequestBody.Companion.asRequestBody
//import java.io.File
//import java.io.FileOutputStream
//import java.io.IOException
//
//
//@AndroidEntryPoint
//class ProfileActivity : AppCompatActivity() {
//    private lateinit var binding: ActivityProfileBinding
//    private val viewModel: ProfileViewModel by viewModels()
//
//    private var getFile: File = File("")
//
//    companion object {
//        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
//        private const val REQUEST_CODE_PERMISSIONS = 10
//        const val REQUEST_IMAGE_CAPTURE = 1
//        const val REQUEST_IMAGE_GALLERY = 2
//
//    }
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        binding = ActivityProfileBinding.inflate(layoutInflater)
//        super.onCreate(savedInstanceState)
//        setContentView(binding.root)
//
//        syaratKetentuan()
//        setupAction()
//
//    }
//
//    private fun setMyButtonEnable() {
//        binding.apply {
//            val nameSet = userName.text
//            finishBtn.isEnabled =
//                nameSet != null && nameSet.toString()
//                    .isNotEmpty()
//        }
//    }
//
//    private fun setupAction() {
//        binding.apply {
//            userName.addTextChangedListener {
//                setMyButtonEnable()
//            }
//
//            finishBtn.setOnClickListener {
//                val file = getFile
//
//                val requestUser =
//                    MultipartBody.Part.createFormData("userName", userName.text.toString())
//                val requestImageFile = file.asRequestBody("image/*".toMediaTypeOrNull())
//                val userImage: MultipartBody.Part = MultipartBody.Part.createFormData(
//                    "userImage",
//                    file.name,
//                    requestImageFile
//                )
//
//                viewModel.doUploadProfile(userImage, requestUser)
//                AlertDialog.Builder(this@ProfileActivity).apply {
//                    setTitle("Yeah!")
//                    setMessage("Successfully uploading name and saveToProfile image. Let's get you in!")
//                    setPositiveButton("Continue") { _, _ ->
//                        val intent = Intent(context, MainActivity::class.java)
//                        intent.flags =
//                            Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
//                        startActivity(intent)
//                        finish()
//                    }
//                    create()
//                    show()
//                }
//            }
//        }
//    }
//
//
//    override fun onRequestPermissionsResult(
//        requestCode: Int,
//        permissions: Array<String>,
//        grantResults: IntArray
//    ) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//        if (requestCode == REQUEST_CODE_PERMISSIONS) {
//            if (!allPermissionsGranted()) {
//                Toast.makeText(
//                    this,
//                    "Tidak mendapatkan permission.",
//                    Toast.LENGTH_SHORT
//                ).show()
//                finish()
//            }
//        }
//    }
//
//    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
//        ContextCompat.checkSelfPermission(baseContext, it) == PackageManager.PERMISSION_GRANTED
//    }
//
//    fun openProfilPicture(view: View) {
//        MaterialAlertDialogBuilder(this@ProfileActivity)
//            .setTitle(resources.getString(R.string.choose_image))
//            .setNegativeButton(resources.getString(R.string.camera)) { dialog, which ->
//                dispatchTakePictureIntent()
//            }
//            .setPositiveButton(resources.getString(R.string.gallery)) { dialog, which ->
//                keGallery()
//            }
//            .show()
//    }
//
//    private fun dispatchTakePictureIntent() {
//        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        try {
//            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
//        } catch (e: ActivityNotFoundException) {
//            Toast.makeText(this, "Failed opening camera", Toast.LENGTH_SHORT).show()
//        }
//    }
//
//    private fun keGallery() {
//        val galleryIntent = Intent(Intent.ACTION_PICK)
//        galleryIntent.type = "image/*"
//        startActivityForResult(galleryIntent, REQUEST_IMAGE_GALLERY)
//    }
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (resultCode == RESULT_OK) {
//            when (requestCode) {
//                REQUEST_IMAGE_CAPTURE -> {
//                    val imageBitmap = data?.extras?.get("data") as? Bitmap
//                    if (imageBitmap != null) {
//                        val imageFile = bitmapToFile(imageBitmap)
//                        getFile = imageFile
//                        if (imageFile != null) {
//                            // Process and display the captured image
//                            Glide.with(applicationContext)
//                                .load(imageFile)
//                                .circleCrop()
//                                .transition(DrawableTransitionOptions.withCrossFade())
//                                .into(binding.userImage)
//                        } else {
//                            Toast.makeText(this, "Failed to save image", Toast.LENGTH_SHORT).show()
//                        }
//                    } else {
//                        Toast.makeText(
//                            this,
//                            "Failed to retrieve image from camera",
//                            Toast.LENGTH_SHORT
//                        ).show()
//                    }
//                }
//
//                REQUEST_IMAGE_GALLERY -> {
//                    val imageUri = data?.extras?.get("data") as? Bitmap
//                    if (imageUri != null) {
//                        val imageFile = bitmapToFile(imageUri)
//                        getFile = imageFile
//                        if (imageFile != null) {
//                            // Process and display the selected image from the gallery
//                            Glide.with(applicationContext)
//                                .load(imageFile)
//                                .circleCrop()
//                                .transition(DrawableTransitionOptions.withCrossFade())
//                                .into(binding.userImage)
//                        } else {
//                            Toast.makeText(
//                                this,
//                                "Failed to retrieve image from gallery",
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//
//    private fun bitmapToFile(bitmap: Bitmap): File {
//        val file = File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "image.jpg")
//        try {
//            val outputStream = FileOutputStream(file)
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outputStream)
//            outputStream.flush()
//            outputStream.close()
//        } catch (e: IOException) {
//            e.printStackTrace()
//        }
//        return file
//    }
//
//    private fun syaratKetentuan() {
//        binding.apply {
//            val text = resources.getString(R.string.terms_and_conditions)
//            val spannableString = SpannableStringBuilder(text)
//
//            val colorSpan1 = ForegroundColorSpan(Color.parseColor("#A0A0A0"))
//            spannableString.setSpan(colorSpan1, 0, 37, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
//
//            val colorSpan2 = ForegroundColorSpan(Color.parseColor("#6750A4"))
//            spannableString.setSpan(colorSpan2, 37, 55, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
//
//            val colorSpan3 = ForegroundColorSpan(Color.parseColor("#A0A0A0"))
//            spannableString.setSpan(colorSpan3, 56, 62, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
//
//            val colorSpan4 = ForegroundColorSpan(Color.parseColor("#6750A4"))
//            spannableString.setSpan(colorSpan4, 63, 80, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
//
//            val colorSpan5 = ForegroundColorSpan(Color.parseColor("#A0A0A0"))
//            spannableString.setSpan(colorSpan5, 81, 92, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
//
//            syaratKetentuan.text = spannableString
//        }
//    }
//}