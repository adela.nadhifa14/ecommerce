package com.adel.ecommerce.ui.store.filter//package com.adel.ecommerce.ui.store
//
//import android.view.LayoutInflater
//import android.view.ViewGroup
//import androidx.recyclerview.widget.RecyclerView
//import com.bumptech.glide.Glide
//import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
//
//class ChipFilterAdapter : RecyclerView.Adapter<ChipFilterAdapter.SearchViewHolder>() {
//
//    private lateinit var onItemClickCallback: OnItemClickCallback
//
//    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
//        this.onItemClickCallback = onItemClickCallback
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
//        val binding = ItemUserBinding.inflate(LayoutInflater.from(parent.context), parent, false)
//        return SearchViewHolder(binding)
//    }
//
//    @Suppress("DEPRECATION")
//    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
//        val (login, id, ava) = searchUser[position]
//        holder.binding.tvSearchUsername.text = StringBuilder().append("@").append(login)
//        holder.binding.tvSearchId.text = id.toString()
//        Glide.with(holder.itemView.context)
//            .load(ava)
//            .circleCrop()
//            .transition(DrawableTransitionOptions.withCrossFade())
//            .into(holder.binding.imgSearchAvatar)
//        holder.binding.imgSearchAvatar.setOnClickListener {
//            onItemClickCallback.onItemClicked(searchUser[holder.adapterPosition])
//        }
//    }
//
//    override fun getItemCount() = searchUser.size
//
//    class SearchViewHolder(var binding: ItemUserBinding) : RecyclerView.ViewHolder(binding.root)
//
//    interface OnItemClickCallback {
//        fun onItemClicked(data: User)
//    }
//}
