package com.adel.ecommerce.ui.store.filter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.adel.ecommerce.databinding.BottomLoadingBinding

class BottomLoadingStateAdapter :
    LoadStateAdapter<BottomLoadingStateAdapter.LoadingStateViewHolder>() {

    inner class LoadingStateViewHolder(private val binding: BottomLoadingBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(loadState: LoadState) {
            binding.bottomProgressBar.visibility =
                if (loadState is LoadState.Loading) View.VISIBLE else View.GONE
        }
    }

    override fun onBindViewHolder(holder: LoadingStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): LoadingStateViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = BottomLoadingBinding.inflate(inflater, parent, false)
        return LoadingStateViewHolder(binding)
    }
}
