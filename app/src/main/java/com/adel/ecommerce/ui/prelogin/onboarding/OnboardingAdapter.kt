package com.adel.ecommerce.ui.prelogin.onboarding

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.adel.ecommerce.databinding.SliderItemBinding
import com.bumptech.glide.Glide

class OnboardingAdapter(private val imageList: Array<Int>) :
    RecyclerView.Adapter<OnboardingAdapter.ImageViewHolder>() {

    inner class ImageViewHolder(itemView: SliderItemBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        private val binding = itemView
        fun bind(data: Int) {
            with(binding) {
                Glide.with(itemView)
                    .load(data)
                    .into(imageView)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return ImageViewHolder(
            SliderItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = imageList.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bind(imageList[position])
    }
}