package com.adel.ecommerce.ui.store.notification

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.adel.ecommerce.service.NotificationProducts
import com.adel.ecommerce.service.WishlistProducts

@Dao
interface NotificationDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addToNotification(notificationProducts: NotificationProducts): Long

    @Query("SELECT * FROM notification_products ORDER BY date DESC")
    fun getNotificationProducts(): LiveData<List<NotificationProducts>>

    @Query("UPDATE notification_products SET isRead = :isRead WHERE id IN (:id)")
    suspend fun isReadNotification(isRead: Boolean, id: String): Int
}