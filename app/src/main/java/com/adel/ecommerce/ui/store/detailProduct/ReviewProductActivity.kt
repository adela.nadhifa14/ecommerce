package com.adel.ecommerce.ui.store.detailProduct

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.adel.ecommerce.EcommerceRepository
import com.adel.ecommerce.databinding.ActivityReviewProductBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ReviewProductActivity : AppCompatActivity() {
    private lateinit var binding: ActivityReviewProductBinding
    private lateinit var adapter: ReviewProductAdapter

    @Inject
    lateinit var repository: EcommerceRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityReviewProductBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val id = intent.getStringExtra(DetailProductActivity.EXTRA_ID_DETAIL)
        val bundle = Bundle()
        bundle.putString(DetailProductActivity.EXTRA_ID_DETAIL, id)

        id?.let {
            repository.getReviewBuyer(it)
        }

        adapter = ReviewProductAdapter()
        adapter.notifyDataSetChanged()

        repository.reviewBuyer.observe(this) {
            if (it != null) {
                binding.apply {
                    adapter.setReviewBuyer(it)
                }
            }

            val layoutManager = LinearLayoutManager(this)
            binding.recyclerView2.layoutManager = layoutManager
            binding.recyclerView2.addItemDecoration(
                DividerItemDecoration(
                    this,
                    layoutManager.orientation
                )
            )
            binding.recyclerView2.setHasFixedSize(true)
            binding.recyclerView2.adapter = adapter
            Log.d(TAG, "it response : $it")
        }

        binding.topAppBar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    companion object {
        const val TAG = "ReviewProductActivity"
        const val EXTRA_ID_REVIEW = "extra_id"
    }
}