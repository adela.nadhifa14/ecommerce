package com.adel.ecommerce.ui.store.notification

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.adel.ecommerce.databinding.ActivityNotificationBinding
import com.adel.ecommerce.service.NotificationProducts
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NotificationActivity : AppCompatActivity() {
    private lateinit var binding: ActivityNotificationBinding
    private lateinit var adapter: NotificationAdapter
    private val viewModel: NotificationViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityNotificationBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        adapter = NotificationAdapter(viewModel)
        binding.rvNotification.layoutManager = GridLayoutManager(this, 1)
        binding.rvNotification.adapter = adapter

        viewModel.getNotificationItem()?.observe(this) {
            Log.d("Notification", "onCreate: $it")
            if (it != null) {
                val productList: List<NotificationProducts> = it

                val arrayList: ArrayList<NotificationProducts> = ArrayList(productList)

                adapter.setNotifItem(arrayList)
                adapter.submitList(it)
            }
        }

        binding.topAppBar.setNavigationOnClickListener {
            onBackPressed()
        }
    }
}