package com.adel.ecommerce.ui.prelogin.profile

import androidx.lifecycle.ViewModel
import com.adel.ecommerce.EcommerceRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import okhttp3.MultipartBody
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val repository: EcommerceRepository
) : ViewModel() {

    fun doUploadProfile(
        userName: MultipartBody.Part,
        userImage: MultipartBody.Part?
    ) {
        return repository.profile(userImage!!, userName)
    }


}