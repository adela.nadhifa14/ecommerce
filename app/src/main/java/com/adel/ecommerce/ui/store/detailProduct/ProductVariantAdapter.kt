package com.adel.ecommerce.ui.store.detailProduct

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.adel.ecommerce.databinding.ActivityDetailProductBinding
import com.adel.ecommerce.service.ProductVariant

class ProductVariantAdapter(private val pv: List<ProductVariant>) :
    RecyclerView.Adapter<ProductVariantAdapter.ProductVariantViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductVariantViewHolder {
        val binding =
            ActivityDetailProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductVariantViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductVariantViewHolder, position: Int) {
        val (variantName, variantPrice) = pv[position]
        holder.binding.chipGroupVarian.addView(variantName as View)


    }

    override fun getItemCount(): Int {
        return pv.size
    }

    class ProductVariantViewHolder(var binding: ActivityDetailProductBinding) :
        RecyclerView.ViewHolder(binding.root)
}