package com.adel.ecommerce.ui.store.pagingStore

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.adel.ecommerce.service.ApiService
import com.adel.ecommerce.service.GetProductsItemResponse
import javax.inject.Inject

class ProductsPagingSource @Inject constructor(
    private val apiService: ApiService,
    val search: String?,
    private val brand: String?,
    private val lowest: Int?,
    private val highest: Int?,
    private val sort: String?
) : PagingSource<Int, GetProductsItemResponse>() {
    override suspend fun load(
        params: LoadParams<Int>
    ): LoadResult<Int, GetProductsItemResponse> {
        return try {
            // Start refresh at page 1 if undefined.
            val page = params.key ?: 1
            val limit = params.loadSize
            val response = apiService.getProducts(search, brand, lowest, highest, sort, limit, page)
            Log.d("Source", "data : ${response.data.items}")
            LoadResult.Page(
                data = response.data.items,
                prevKey = null, // Only paging forward.
                nextKey = if (page == response.data.totalPages) null else page + 1
            )
        } catch (exception: Exception) {
            LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, GetProductsItemResponse>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }
}