package com.adel.ecommerce.utils

sealed interface DataState<out T> {
    data class Success<out T>(val data: T) : DataState<T>
    data class Error(val exception: Throwable? = null) : DataState<Nothing>
    object Loading : DataState<Nothing>
    object Init : DataState<Nothing>
}

