package com.adel.ecommerce.service

import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @Headers("Content-type:application/json; charset=utf-8")
    @POST("register")
    fun doRegister(
        @Body user: Auth
    ): Call<DataResponse>

    @Headers("Content-type:application/json; charset=utf-8")
    @POST("login")
    fun doLogin(
        @Body user: Auth
    ): Call<DataResponse>

    @Multipart
    @POST("profile")
    fun saveToProfile(
        @Part userName: MultipartBody.Part,
        @Part userImage: MultipartBody.Part?
    ): Call<ProfileResponse>

    @POST("refresh")
    fun refreshToken(
        @Body token: TokenRequest
    ): Call<RefreshResponse>

    @POST("search")
    fun doSearch(
        @Query("query") query: String
    ): Call<SearchResponse>

    @GET("products/{id}")
    fun getProductDetail(
        @Path("id") id: String
    ): Call<GetProductDetailResponse>

    @GET("review/{id}")
    fun getReviewBuyer(
        @Path("id") id: String
    ): Call<ReviewBuyerResponse>

    @POST("products")
    suspend fun getProducts(
        @Query("search") search: String?,
        @Query("brand") brand: String?,
        @Query("lowest") lowest: Int?,
        @Query("highest") highest: Int?,
        @Query("sort") sort: String?,
        @Query("limit") limit: Int?,
        @Query("page") page: Int?,
    ): GetProductResponse

    @GET("payment")
    fun getPaymentMethods(): Call<PaymentMethodResponse>

    @POST("fulfillment")
    fun doBuyProducts(
        @Body payment : Payment
    ): Call<PaymentResponse>

    @POST("rating")
    fun doGiveRating(
        @Body rating : Rating
    ): Call<RatingResponse>

    @GET("transaction")
    fun getTransactionHistory(): Call<TransactionResponse>
}