package com.adel.ecommerce.service

import com.adel.ecommerce.session.SessionPreference
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthInterceptor @Inject constructor(
    private val session: SessionPreference
) : Interceptor {

    val API_KEY = "6f8856ed-9189-488f-9011-0ff4b6c08edc"
    override fun intercept(chain: Interceptor.Chain): Response {
        val endpoint = chain.request().url.encodedPath
        val accessToken = session.passAccessToken().toString()

        val requestBuilder = chain.request().newBuilder()
            .addHeader("API_KEY", API_KEY)

        if (!endpoint.contains("/login") && !endpoint.contains("/register") && !endpoint.contains("/refresh")) {
            requestBuilder.addHeader("Authorization", "Bearer $accessToken")
        }

        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}