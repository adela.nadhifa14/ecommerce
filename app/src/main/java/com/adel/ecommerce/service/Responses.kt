package com.adel.ecommerce.service

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue
import java.util.Date

data class Auth(
    val email: String,
    val password: String,
    val firebaseToken: String
)

data class DataResponse(
    val code: Int,
    val message: String,
    val data: ResultResponse?
)

data class ResultResponse(
    val userName: String,
    val userImage: String,
    val accessToken: String,
    val refreshToken: String,
    val expiresAt: Int
)

data class ProfileResponse(
    val code: Int,
    val message: String,
    val data: ProfileResultResponse?
)

data class ProfileResultResponse(
    val userName: String,
    val userImage: String
)

data class TokenRequest(
    @field:SerializedName("token")
    val token: String
)

data class RefreshResponse(
    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("message")
    val message: String,

    @field:SerializedName("data")
    val data: RefreshDataResponse?
)

data class RefreshDataResponse(
    @field:SerializedName("accessToken")
    val accessToken: String,

    @field:SerializedName("refreshToken")
    val refreshToken: String,

    @field:SerializedName("expiresAt")
    val expiresAt: Int
)

data class SearchResponse(
    val code: Int,
    val message: String,
    val data: List<String>
)

data class GetProductResponse(
    val code: Int,
    val message: String,
    val data: GetProductsResultResponse
)

data class GetProductsResultResponse(
    val itemsPerPage: Int,
    val currentItemCount: Int,
    val pageIndex: Int,
    val totalPages: Int,
    val items: List<GetProductsItemResponse>
)

data class GetProductsItemResponse(
    val productId: String,
    val productName: String,
    val productPrice: Int,
    val image: String,
    val brand: String,
    val store: String,
    val sale: Int,
    val productRating: Float
)

data class GetProductDetailResponse(
    val code: Int,
    val message: String,
    val data: GetProductDetailItemResponse
)

data class GetProductDetailItemResponse(
    val productId: String,
    val productName: String,
    val productPrice: Int,
    val image: List<String>,
    val brand: String,
    val description: String,
    val store: String,
    val sale: Int,
    val stock: Int,
    val totalRating: Int,
    val totalReview: Int,
    val totalSatisfaction: Int,
    val productRating: Float,
    val productVariant: List<ProductVariant>
)

data class ProductVariant(
    val variantName: String,
    val variantPrice: Int
)

data class ReviewBuyerResponse(
    val code: Int,
    val message: String,
    val data: ArrayList<ReviewBuyerDataResponse>
)

data class ReviewBuyerDataResponse(
    val userName: String,
    val userImage: String,
    val userRating: Int,
    val userReview: String
)

@Entity(tableName = "wishlist_products")
@Parcelize
data class WishlistProducts(
    @PrimaryKey
    val productId: String,
    val productName: String,
    val productPrice: Int,
    val image: String,
    val brand: String,
    val description: String,
    val store: String,
    val sale: Int,
    val stock: Int?,
    val totalRating: Int,
    val totalReview: Int,
    val totalSatisfaction: Int,
    val productRating: Float,
    var variantName: String,
    var variantPrice: Int?,
    var quantity: Int = 1,
    var selected: Boolean = false
) : Parcelable

fun GetProductDetailItemResponse.asWishlist(
    variantName: String?,
    variantPrice: Int?
): WishlistProducts {
    return WishlistProducts(
        productId,
        productName,
        productPrice,
        image.firstOrNull().toString(),
        brand,
        description,
        store,
        sale,
        stock,
        totalRating,
        totalReview,
        totalSatisfaction,
        productRating,
        variantName.toString(),
        variantPrice
    )
}

data class PaymentMethodResponse(
    val code: Int,
    val message: String,
    val data: List<PaymentMethodCategoryResponse>
)

data class PaymentMethodCategoryResponse(
    val title: String,
    val item: List<PaymentMethodItemResponse>
)

data class PaymentMethodItemResponse(
    val label: String,
    val image: String,
    val status: Boolean
)

data class Payment(
    val payment: String,
    val items: @RawValue List<PaymentItem>
)

@Parcelize
data class PaymentItem(
    val productId: String,
    val variantName: String,
    val quantity: Int
) : Parcelable

@Parcelize
data class PaymentResponse(
    val code: Int,
    val message: String,
    val data: @RawValue PaymentDataResponse
) : Parcelable

@Parcelize
data class PaymentDataResponse(
    val invoiceId: String,
    val status: Boolean,
    val date: String,
    val time: String,
    val payment: String,
    val total: Int
) : Parcelable

data class Rating(
    val invoiceId: String,
    val rating: Int?,
    val review: String?
)

data class RatingResponse(
    val code: String,
    val message: String
)

data class TransactionResponse(
    val code: String,
    val message: String,
    val data: List<TransactionDataResponse>
)

@Parcelize
data class TransactionDataResponse(
    val invoiceId: String,
    val status: Boolean,
    val date: String,
    val time: String,
    val payment: String,
    val total: Int,
    val items: @RawValue List<PaymentItem>?,
    val rating: Int,
    val review: String?,
    val image: String,
    val name: String
) : Parcelable

@Entity(tableName = "notification_products")
@Parcelize
data class NotificationProducts(
    @PrimaryKey
    val id: String,
    val title: String,
    val body: String,
    val date: String,
    val image: String,
    val type: String,
    val isRead: Boolean
) : Parcelable