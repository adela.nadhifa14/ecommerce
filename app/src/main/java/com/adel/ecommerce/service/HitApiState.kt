package com.adel.ecommerce.service


sealed class HitApiState {
    object Loading : HitApiState()
    data class Success(val responseData: Any) : HitApiState()
    data class Error(val message: String) : HitApiState()
}
