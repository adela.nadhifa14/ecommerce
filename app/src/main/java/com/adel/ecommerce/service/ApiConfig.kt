package com.adel.ecommerce.service

import android.app.Application
import android.content.Context
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApiConfig {

    private val BASE_URL_WORK = "http://172.17.20.216:8080/"
//    private val BASE_URL_HUAWEI = "http://192.168.8.107:8080/"
//    private val BASE_URL_HP = "http://172.20.10.14:8080/"//
//    private val BASE_URL_AIR_BIRU = "http://192.168.0.105:8080/"


    private val API_KEY = "6f8856ed-9189-488f-9011-0ff4b6c08edc"

    @Singleton
    @Provides
    fun getChucker(
        @ApplicationContext context: Context
    ): ChuckerInterceptor {
        return ChuckerInterceptor.Builder(context)
            .build()
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(chucker: ChuckerInterceptor, authInterceptor: AuthInterceptor, authenticator: AuthAuthenticator): OkHttpClient {
        val client = OkHttpClient().newBuilder()

        client.addInterceptor(HttpLoggingInterceptor()
            .apply { setLevel(HttpLoggingInterceptor.Level.BODY) })
            .addNetworkInterceptor(
                Interceptor {
                    it.proceed(
                        it.request()
                            .newBuilder()
                            .addHeader("API_KEY", API_KEY)
                            .build()
                    )
                })
            .addInterceptor(chucker)
            .addInterceptor(authInterceptor)
            .authenticator(authenticator)
            .build()

        return client.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): ApiService {
        val retrofitE = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL_WORK)
            .client(okHttpClient)
            .build()

        return retrofitE.create(ApiService::class.java)
    }
}