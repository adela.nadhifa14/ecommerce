package com.adel.ecommerce.service

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.adel.ecommerce.session.SessionPreference
import com.chuckerteam.chucker.api.ChuckerInterceptor
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class AuthAuthenticator @Inject constructor(
    private val session: SessionPreference,
    private val chuckerInterceptor: ChuckerInterceptor
) : Authenticator {

    private val BASE_URL = "http://172.17.20.216:8080/"
    private val API_KEY = "6f8856ed-9189-488f-9011-0ff4b6c08edc"

    private val logoutLiveData: MutableLiveData<Unit> = MutableLiveData()

    val logoutEvent: LiveData<Unit>
        get() = logoutLiveData


    override fun authenticate(route: Route?, response: Response): Request? {
        val refreshToken = session.passRefreshToken().toString()
        Log.d("TAG_PRO", "Bro : $refreshToken")
        synchronized(this) {
            return runBlocking {
                try {
                    Log.d("TAG_PRO", "Bro authenticate")
                    val newToken = refreshToken(TokenRequest(refreshToken))
                    if (!newToken.isEmpty()) {
                        session.saveAccessToken(newToken)
                        response.request
                            .newBuilder()
                            .header("Authorization", "Bearer $newToken")
                            .build()
                    } else {
                        logoutLiveData.postValue(Unit)
                        null
                    }
                } catch (
                    error: Throwable
                ) {
                    Log.d("TAG_PRO", "authenticate: $error")
                    null
                }
            }
        }
    }

    private fun refreshToken(tokenRequest: TokenRequest): String {
        val interceptor = Interceptor.invoke { chain ->
            val request = chain
                .request()
                .newBuilder()
                .addHeader("API_KEY", API_KEY)
                .build()
            chain.proceed(request)
        }
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(chuckerInterceptor)
            .addInterceptor(interceptor)
            .build()
        val apiService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(ApiService::class.java)

        val call = apiService.refreshToken(tokenRequest)
        val response = call.execute()
        if (response.isSuccessful) {
            val access = response.body()?.data ?: throw Exception("Data Empty")
            return access.accessToken
        } else {
            throw Exception(response.code().toString())
        }
    }
}