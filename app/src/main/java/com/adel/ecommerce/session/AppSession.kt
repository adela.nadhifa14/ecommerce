package com.adel.ecommerce.session

import android.content.Context
import android.content.SharedPreferences

object AppSession {
    private const val PREFS_NAME = "MyAppPreferences"
    private const val KEY_FIRST_INSTALL = "isFirstInstall"

    private lateinit var preferences: SharedPreferences

    fun init(context: Context) {
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }

    var isFirstInstall: Boolean
        get() = preferences.getBoolean(KEY_FIRST_INSTALL, true)
        set(value) = preferences.edit().putBoolean(KEY_FIRST_INSTALL, value).apply()
}