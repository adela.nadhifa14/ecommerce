package com.adel.ecommerce.session

import android.content.Context
import android.content.Context.MODE_PRIVATE
import com.adel.ecommerce.R
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class SessionPreference @Inject constructor(@ApplicationContext context: Context) {
    companion object {
        private const val ACCESS_TOKEN = "access_token"
        private const val FIREBASE_TOKEN = "firebase_token"
        private const val REFRESH_TOKEN = "refresh_token"
        private const val USER_IMG = "image"
        private const val USER_NAME = "name"
    }

    private val sessionPref =
        context.getSharedPreferences(context.getString(R.string.app_name), MODE_PRIVATE)

    fun saveAccessToken(token: String) {
        val editor = sessionPref.edit()
        editor.putString(ACCESS_TOKEN, token)
        editor.apply()
    }

    fun saveRefreshToken(token: String) {
        val editor = sessionPref.edit()
        editor.putString(REFRESH_TOKEN, token)
        editor.apply()
    }

    fun passAccessToken(): String? {
        return sessionPref.getString(ACCESS_TOKEN, null)
    }

    fun passRefreshToken(): String? {
        return sessionPref.getString(REFRESH_TOKEN, null)
    }

    fun saveUserImage(img: String) {
        val editor = sessionPref.edit()
        editor.putString(USER_IMG, img)
        editor.apply()
    }

    fun saveUserName(name: String) {
        val editor = sessionPref.edit()
        editor.putString(USER_NAME, name)
        editor.apply()
    }

    fun passUserImage(): String? {
        return sessionPref.getString(USER_IMG, null)
    }

    fun passUserName(): String? {
        return sessionPref.getString(USER_NAME, null)
    }

    fun logOut() {
        sessionPref.edit().clear().apply()
    }
}