package com.adel.ecommerce

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import com.adel.ecommerce.service.ApiService
import com.adel.ecommerce.service.Auth
import com.adel.ecommerce.service.DataResponse
import com.adel.ecommerce.service.GetProductDetailItemResponse
import com.adel.ecommerce.service.GetProductDetailResponse
import com.adel.ecommerce.service.GetProductsItemResponse
import com.adel.ecommerce.service.Payment
import com.adel.ecommerce.service.PaymentDataResponse
import com.adel.ecommerce.service.PaymentMethodCategoryResponse
import com.adel.ecommerce.service.PaymentMethodResponse
import com.adel.ecommerce.service.PaymentResponse
import com.adel.ecommerce.service.ProfileResponse
import com.adel.ecommerce.service.Rating
import com.adel.ecommerce.service.RatingResponse
import com.adel.ecommerce.service.ReviewBuyerDataResponse
import com.adel.ecommerce.service.ReviewBuyerResponse
import com.adel.ecommerce.service.TransactionDataResponse
import com.adel.ecommerce.service.TransactionResponse
import com.adel.ecommerce.session.SessionPreference
import com.adel.ecommerce.ui.store.pagingStore.ProductsPagingSource
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class EcommerceRepository @Inject constructor(
    private val apiService: ApiService,
    private val session: SessionPreference
) {

    fun register(auth: Auth) {
        apiService.doRegister(auth).enqueue(object : Callback<DataResponse> {
            override fun onResponse(
                call: Call<DataResponse>,
                response: Response<DataResponse>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    session.saveAccessToken(response.body()!!.data!!.accessToken)
                    session.saveRefreshToken(response.body()!!.data!!.refreshToken)
                    Log.d(TAG, "Response : ${response.body()}}")

                }
            }

            override fun onFailure(call: Call<DataResponse>, t: Throwable) {
                Log.d(TAG, "Response : ${t.message.toString()}")
            }
        })
    }

    fun profile(
        userName: MultipartBody.Part,
        userImage: MultipartBody.Part?
    ) {
        apiService.saveToProfile(userName, userImage)
            .enqueue(object : Callback<ProfileResponse> {
                override fun onResponse(
                    call: Call<ProfileResponse>,
                    response: Response<ProfileResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {
//                        session.saveUserImage(response.body()!!.data!!.userImage)
                        session.saveUserName(response.body()!!.data!!.userName)
                    }
                }

                override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                    Log.d(TAG, "Response : ${t.message.toString()}")
                }
            })
    }

    fun getProductPaging(
        search: String?,
        brand: String?,
        lowest: Int?,
        highest: Int?,
        sort: String?
    ): LiveData<PagingData<GetProductsItemResponse>> {
        return Pager(
            PagingConfig(
                pageSize = 10, initialLoadSize = 10, prefetchDistance = 1
            )
        ) {
            ProductsPagingSource(apiService, search, brand, lowest, highest, sort)
        }.liveData
    }

    val _detailProduct = MutableLiveData<GetProductDetailItemResponse>()
    val detailProduct: LiveData<GetProductDetailItemResponse> = _detailProduct
    fun getProductDetail(
        id: String
    ) {
        apiService.getProductDetail(id).enqueue(object : Callback<GetProductDetailResponse> {
            override fun onResponse(
                call: Call<GetProductDetailResponse>,
                response: Response<GetProductDetailResponse>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    _detailProduct.postValue(response.body()?.data)
                    Log.d(TAG, "response : $response")
                }
            }

            override fun onFailure(call: Call<GetProductDetailResponse>, t: Throwable) {
                Log.d(TAG, "onFailure : ${t.message.toString()}")
            }

        })
    }

    val _reviewBuyer = MutableLiveData<ArrayList<ReviewBuyerDataResponse>>()
    val reviewBuyer: LiveData<ArrayList<ReviewBuyerDataResponse>> = _reviewBuyer
    fun getReviewBuyer(
        id: String
    ) {
        apiService.getReviewBuyer(id).enqueue(object : Callback<ReviewBuyerResponse> {
            override fun onResponse(
                call: Call<ReviewBuyerResponse>,
                response: Response<ReviewBuyerResponse>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    _reviewBuyer.value = response.body()?.data
                    Log.d(TAG, "response : $response")
                }
            }

            override fun onFailure(call: Call<ReviewBuyerResponse>, t: Throwable) {
                Log.d(TAG, "onFailure : ${t.message.toString()}")
            }

        })
    }

    val _paymentMethods = MutableLiveData<List<PaymentMethodCategoryResponse>>()
    val paymentMethods: LiveData<List<PaymentMethodCategoryResponse>> = _paymentMethods
    fun getPaymentMethods() {
        apiService.getPaymentMethods().enqueue(object : Callback<PaymentMethodResponse> {
            override fun onResponse(
                call: Call<PaymentMethodResponse>,
                response: Response<PaymentMethodResponse>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    _paymentMethods.value = response.body()!!.data
                    Log.d(TAG, "response : $response")
                }
            }

            override fun onFailure(call: Call<PaymentMethodResponse>, t: Throwable) {
                Log.d(TAG, "onFailure : ${t.message.toString()}")
            }

        })
    }

    val _payment = MutableLiveData<PaymentDataResponse>()
    val payment: LiveData<PaymentDataResponse> = _payment

    fun payment(payment: Payment) {
        apiService.doBuyProducts(payment).enqueue(object : Callback<PaymentResponse> {
            override fun onResponse(
                call: Call<PaymentResponse>,
                response: Response<PaymentResponse>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    _payment.value = response.body()!!.data
                    Log.d(TAG, "response : ${response.body()!!.data}")
                }
            }

            override fun onFailure(call: Call<PaymentResponse>, t: Throwable) {
                Log.d(TAG, "onFailure : ${t.message.toString()}")
            }
        })
    }


    val _rating = MutableLiveData<String>()
    val rating: LiveData<String> = _rating
    fun rating(rating: Rating) {
        apiService.doGiveRating(rating).enqueue(object : Callback<RatingResponse> {
            override fun onResponse(
                call: Call<RatingResponse>,
                response: Response<RatingResponse>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    _rating.value = response.body()!!.message
                    Log.d(TAG, "response : ${response.body()!!.message}")
                }
            }

            override fun onFailure(call: Call<RatingResponse>, t: Throwable) {
                Log.d(TAG, "onFailure : ${t.message.toString()}")
            }
        })
    }

    val _transaction = MutableLiveData<List<TransactionDataResponse>>()
    val transaction: LiveData<List<TransactionDataResponse>> = _transaction
    fun getTransaction() {
        apiService.getTransactionHistory().enqueue(object : Callback<TransactionResponse> {
            override fun onResponse(
                call: Call<TransactionResponse>,
                response: Response<TransactionResponse>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    _transaction.value = response.body()!!.data
                    Log.d(TAG, "response : $response")
                }
            }

            override fun onFailure(call: Call<TransactionResponse>, t: Throwable) {
                Log.d(TAG, "onFailure : ${t.message.toString()}")
            }

        })
    }

    companion object {
        private const val TAG = "Repository"
    }
}